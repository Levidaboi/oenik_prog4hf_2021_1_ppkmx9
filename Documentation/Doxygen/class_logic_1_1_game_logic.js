var class_logic_1_1_game_logic =
[
    [ "GameLogic", "class_logic_1_1_game_logic.html#ae475c4c00d4eea9e8a9f7a70813695ca", null ],
    [ "AddGame", "class_logic_1_1_game_logic.html#a92bf9c6355e2d687f6cd0279b0840819", null ],
    [ "DeleteGame", "class_logic_1_1_game_logic.html#a3402e7815e676f4232a9fc091bee3807", null ],
    [ "GetAchiPoints", "class_logic_1_1_game_logic.html#a295d022e3a56d50b2e7343ff65902b5d", null ],
    [ "GetAllGames", "class_logic_1_1_game_logic.html#abea1d3f39e4523ad73765240958c71ed", null ],
    [ "GetGame", "class_logic_1_1_game_logic.html#aa83776f651fd984c38e87d219a945bae", null ],
    [ "SumGameTime", "class_logic_1_1_game_logic.html#a225b6d583714fbfb21b970eb2d4149be", null ],
    [ "UpdateGame", "class_logic_1_1_game_logic.html#a2e523011cc35425a19ca72071cf9da46", null ]
];