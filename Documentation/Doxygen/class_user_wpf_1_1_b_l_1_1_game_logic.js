var class_user_wpf_1_1_b_l_1_1_game_logic =
[
    [ "GameLogic", "class_user_wpf_1_1_b_l_1_1_game_logic.html#ab407d7618a8ebadd3d6124af8c36abef", null ],
    [ "AddGame", "class_user_wpf_1_1_b_l_1_1_game_logic.html#aa78d5e775158f981c9a5301b228f9ec0", null ],
    [ "DelGame", "class_user_wpf_1_1_b_l_1_1_game_logic.html#a5e7bf9f8856f9296f2d73d3f3d635728", null ],
    [ "GetAllGames", "class_user_wpf_1_1_b_l_1_1_game_logic.html#ae139dd680c2b5fcb170857aae04b6a94", null ],
    [ "ModGame", "class_user_wpf_1_1_b_l_1_1_game_logic.html#aef7a2fddbfac95a09ebdb41f2afa1ea3", null ]
];