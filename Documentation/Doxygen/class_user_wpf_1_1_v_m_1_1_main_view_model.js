var class_user_wpf_1_1_v_m_1_1_main_view_model =
[
    [ "MainViewModel", "class_user_wpf_1_1_v_m_1_1_main_view_model.html#a6e2a7cc30363cd8343d5d4cf203c762e", null ],
    [ "MainViewModel", "class_user_wpf_1_1_v_m_1_1_main_view_model.html#acf843aa4b7287b66c1582ec9f1dcd2e6", null ],
    [ "AddCmd", "class_user_wpf_1_1_v_m_1_1_main_view_model.html#a48067ab49e7f71c94c8aba33fc833201", null ],
    [ "DelCmd", "class_user_wpf_1_1_v_m_1_1_main_view_model.html#a554cc4448107a5be119172c004a68614", null ],
    [ "Library", "class_user_wpf_1_1_v_m_1_1_main_view_model.html#a77e18db99041f6aaaa6f8905cc26b3cd", null ],
    [ "ModCmd", "class_user_wpf_1_1_v_m_1_1_main_view_model.html#afb9460f7b378424615e078ef4764b00e", null ],
    [ "SelectedGame", "class_user_wpf_1_1_v_m_1_1_main_view_model.html#aeb22c8c6e1ca0f2074c17894a4082268", null ]
];