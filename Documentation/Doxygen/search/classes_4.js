var searchData=
[
  ['ieditorservice_167',['IEditorService',['../interface_user_wpf_1_1_b_l_1_1_i_editor_service.html',1,'UserWpf::BL']]],
  ['igamelogic_168',['IGameLogic',['../interface_user_wpf_1_1_b_l_1_1_i_game_logic.html',1,'UserWpf::BL']]],
  ['irepo_169',['IRepo',['../interface_repository_1_1_i_repo.html',1,'Repository']]],
  ['irepo_3c_20achievement_20_3e_170',['IRepo&lt; Achievement &gt;',['../interface_repository_1_1_i_repo.html',1,'Repository']]],
  ['irepo_3c_20game_20_3e_171',['IRepo&lt; Game &gt;',['../interface_repository_1_1_i_repo.html',1,'Repository']]],
  ['irepo_3c_20models_3a_3aachievement_20_3e_172',['IRepo&lt; Models::Achievement &gt;',['../interface_repository_1_1_i_repo.html',1,'Repository']]],
  ['irepo_3c_20models_3a_3agame_20_3e_173',['IRepo&lt; Models::Game &gt;',['../interface_repository_1_1_i_repo.html',1,'Repository']]],
  ['irepo_3c_20models_3a_3auser_20_3e_174',['IRepo&lt; Models::User &gt;',['../interface_repository_1_1_i_repo.html',1,'Repository']]],
  ['irepo_3c_20user_20_3e_175',['IRepo&lt; User &gt;',['../interface_repository_1_1_i_repo.html',1,'Repository']]]
];
