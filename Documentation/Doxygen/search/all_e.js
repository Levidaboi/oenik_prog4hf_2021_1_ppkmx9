var searchData=
[
  ['rating_108',['Rating',['../class_models_1_1_game.html#a0ee59b01879590a2de43209587b7d019',1,'Models.Game.Rating()'],['../class_user_wpf_1_1_data_1_1_game.html#a30a405db4157948c3406b17f5993ac9f',1,'UserWpf.Data.Game.Rating()']]],
  ['read_109',['Read',['../class_repository_1_1_achievement_repo.html#abc605502ab0a3f921eedab9ed5e56de0',1,'Repository.AchievementRepo.Read()'],['../class_repository_1_1_game_repo.html#a704841907043af2fbf4af6d73e611613',1,'Repository.GameRepo.Read()'],['../interface_repository_1_1_i_repo.html#a6771da98848fb3c6756c7318222a7f09',1,'Repository.IRepo.Read()'],['../class_repository_1_1_user_repo.html#a684813cce320e4f0372489ed6675f539',1,'Repository.UserRepo.Read()']]],
  ['repository_110',['Repository',['../namespace_repository.html',1,'']]],
  ['richestgamer_111',['RichestGamer',['../class_models_1_1_stat2.html#aab298090b56d7a21d86a3601ea77d6ae',1,'Models::Stat2']]]
];
