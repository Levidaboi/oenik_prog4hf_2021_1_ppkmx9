var searchData=
[
  ['bl_124',['BL',['../namespace_user_wpf_1_1_b_l.html',1,'UserWpf']]],
  ['data_125',['Data',['../namespace_user_wpf_1_1_data.html',1,'UserWpf']]],
  ['ui_126',['UI',['../namespace_user_wpf_1_1_u_i.html',1,'UserWpf']]],
  ['update_127',['Update',['../class_repository_1_1_achievement_repo.html#af999de9ad65980d889bd990c6e5edd02',1,'Repository.AchievementRepo.Update()'],['../class_repository_1_1_game_repo.html#a2a9f4511ec1e356b379a4bc325668e4c',1,'Repository.GameRepo.Update()'],['../interface_repository_1_1_i_repo.html#a4d5e95970f97e394781b14d05acdfb81',1,'Repository.IRepo.Update()'],['../class_repository_1_1_user_repo.html#a660216accd562a78b124b549112486be',1,'Repository.UserRepo.Update()']]],
  ['updateachi_128',['UpdateAchi',['../class_logic_1_1_achi_logic.html#a1c42dd60390102d098912e9351ed7bbf',1,'Logic::AchiLogic']]],
  ['updategame_129',['UpdateGame',['../class_logic_1_1_game_logic.html#a2e523011cc35425a19ca72071cf9da46',1,'Logic::GameLogic']]],
  ['updateuser_130',['UpdateUser',['../class_logic_1_1_user_logic.html#ad262a18d4c2a4a5a8a79958f655a8ff5',1,'Logic::UserLogic']]],
  ['user_131',['User',['../class_models_1_1_user.html',1,'Models.User'],['../class_models_1_1_game.html#adfff3ed4309b6f4b8f7a60655026c22e',1,'Models.Game.User()']]],
  ['userdbcontext_132',['UserDbContext',['../class_data_1_1_user_db_context.html',1,'Data.UserDbContext'],['../class_data_1_1_user_db_context.html#ac3543d4602d670456f66764714e6c06d',1,'Data.UserDbContext.UserDbContext(DbContextOptions&lt; UserDbContext &gt; opt)'],['../class_data_1_1_user_db_context.html#ac67b8ebb7ad3fd660102be4650478ecd',1,'Data.UserDbContext.UserDbContext()']]],
  ['userid_133',['UserId',['../class_models_1_1_game.html#ab44bc9d7fcade8586799fabaf2e8d5b1',1,'Models.Game.UserId()'],['../class_models_1_1_user.html#a556579524815baac640b66d1e7ed0884',1,'Models.User.UserId()']]],
  ['userlogic_134',['UserLogic',['../class_logic_1_1_user_logic.html',1,'Logic.UserLogic'],['../class_logic_1_1_user_logic.html#a0a0b530f3b049638c99e3e759432f64a',1,'Logic.UserLogic.UserLogic(IRepo&lt; User &gt; userrepo, IRepo&lt; Game &gt; gameRepo, IRepo&lt; Achievement &gt; achievementRepo)'],['../class_logic_1_1_user_logic.html#a2a2932cd83bcabb0f8d61d42b915a147',1,'Logic.UserLogic.UserLogic(IRepo&lt; User &gt; userrepo)'],['../class_logic_1_1_user_logic.html#af2b8e9f76a728dc72fded891bb511080',1,'Logic.UserLogic.UserLogic(IRepo&lt; User &gt; userrepo, GameLogic gameLogic, IRepo&lt; Game &gt; gameRepo, IRepo&lt; Achievement &gt; achievementRepo)']]],
  ['userrepo_135',['UserRepo',['../class_repository_1_1_user_repo.html',1,'Repository']]],
  ['users_136',['Users',['../class_data_1_1_user_db_context.html#aa7d311ac4fe1cd408ca16b2432ac0041',1,'Data::UserDbContext']]],
  ['userwpf_137',['UserWpf',['../namespace_user_wpf.html',1,'']]],
  ['vm_138',['VM',['../namespace_user_wpf_1_1_v_m.html',1,'UserWpf']]]
];
