var searchData=
[
  ['gamelogic_245',['GameLogic',['../class_logic_1_1_game_logic.html#ae475c4c00d4eea9e8a9f7a70813695ca',1,'Logic.GameLogic.GameLogic()'],['../class_user_wpf_1_1_b_l_1_1_game_logic.html#ab407d7618a8ebadd3d6124af8c36abef',1,'UserWpf.BL.GameLogic.GameLogic()']]],
  ['generatedata_246',['GenerateData',['../class_feleves__feladat_1_1_controllers_1_1_home_controller.html#a5f27c5fd7bc8ac965ab0649588481a28',1,'Feleves_feladat::Controllers::HomeController']]],
  ['getachievement_247',['GetAchievement',['../class_logic_1_1_achi_logic.html#ada4602a76ff588d5a0b3d68bed86c291',1,'Logic::AchiLogic']]],
  ['getachievements_248',['GetAchievements',['../class_logic_1_1_achi_logic.html#adf1d7b3b145be3d426803cccfdcab524',1,'Logic::AchiLogic']]],
  ['getachipoints_249',['GetAchiPoints',['../class_logic_1_1_game_logic.html#a295d022e3a56d50b2e7343ff65902b5d',1,'Logic.GameLogic.GetAchiPoints()'],['../class_logic_1_1_user_logic.html#a9b94493d362273fed80399cf673036b0',1,'Logic.UserLogic.GetAchiPoints()']]],
  ['getagame_250',['GetAGame',['../class_test_lib_1_1_testlib2.html#a49933106173dfd05f2c9cbf4c4eae421',1,'TestLib::Testlib2']]],
  ['getallgames_251',['GetAllGames',['../class_logic_1_1_game_logic.html#abea1d3f39e4523ad73765240958c71ed',1,'Logic.GameLogic.GetAllGames()'],['../class_user_wpf_1_1_b_l_1_1_game_logic.html#ae139dd680c2b5fcb170857aae04b6a94',1,'UserWpf.BL.GameLogic.GetAllGames()'],['../interface_user_wpf_1_1_b_l_1_1_i_game_logic.html#ab0c609c4a226c637c2de87068d32fbff',1,'UserWpf.BL.IGameLogic.GetAllGames()']]],
  ['getallusers_252',['GetAllUsers',['../class_test_lib_1_1_testlib2.html#ae92d48d13360cb49eed7fc39ccd7c748',1,'TestLib::Testlib2']]],
  ['getbestgamer_253',['GetBestGamer',['../class_logic_1_1_user_logic.html#acd96baac5b91942ecbc522895f559ae3',1,'Logic.UserLogic.GetBestGamer()'],['../class_test_lib_1_1_testlib2.html#aced6cf982130aca334eea1d27af9a381',1,'TestLib.Testlib2.GetBestGamer()']]],
  ['getgame_254',['GetGame',['../class_logic_1_1_game_logic.html#aa83776f651fd984c38e87d219a945bae',1,'Logic::GameLogic']]],
  ['getgamecount_255',['GetGameCount',['../class_logic_1_1_user_logic.html#a60fe6731054146b991771724212f954f',1,'Logic::UserLogic']]],
  ['getgametime_256',['GetGameTime',['../class_logic_1_1_user_logic.html#aa5c18f79edc514e3f7e0a75d18c2547a',1,'Logic::UserLogic']]],
  ['gethashcode_257',['GetHashCode',['../class_models_1_1_achievement.html#a1be876c9e721effd8fc8070f32259e00',1,'Models.Achievement.GetHashCode()'],['../class_models_1_1_game.html#a8274ffdfa57ca0d7c54e0efc6427755f',1,'Models.Game.GetHashCode()'],['../class_models_1_1_user.html#abc766a120cbb999e4d2bb4d7a3111a21',1,'Models.User.GetHashCode()']]],
  ['getlifelessgamer_258',['GetLifelessGamer',['../class_logic_1_1_user_logic.html#acead56c5846eb6aec52db33a5c35eb9b',1,'Logic.UserLogic.GetLifelessGamer()'],['../class_test_lib_1_1_testlib2.html#ae8668ee115c92614f28e5340b5e76feb',1,'TestLib.Testlib2.GetLifelessGamer()']]],
  ['getpickiestgamer_259',['GetPickiestGamer',['../class_logic_1_1_user_logic.html#a7d222b65ba55fed97cb9e9bcc6c60a83',1,'Logic.UserLogic.GetPickiestGamer()'],['../class_test_lib_1_1_testlib2.html#a73d1724626d37125700d240257d2459d',1,'TestLib.Testlib2.GetPickiestGamer()']]],
  ['getpropertyvalue_260',['GetPropertyValue',['../class_xaml_generated_namespace_1_1_generated_internal_type_helper.html#afdc9fe15b56607d02082908d934480c6',1,'XamlGeneratedNamespace.GeneratedInternalTypeHelper.GetPropertyValue(System.Reflection.PropertyInfo propertyInfo, object target, System.Globalization.CultureInfo culture)'],['../class_xaml_generated_namespace_1_1_generated_internal_type_helper.html#afdc9fe15b56607d02082908d934480c6',1,'XamlGeneratedNamespace.GeneratedInternalTypeHelper.GetPropertyValue(System.Reflection.PropertyInfo propertyInfo, object target, System.Globalization.CultureInfo culture)']]],
  ['getrichestgamer_261',['GetRichestGamer',['../class_logic_1_1_user_logic.html#a50d9f267de1f5fd88b0a137af3749d08',1,'Logic::UserLogic']]],
  ['gettestlogic_262',['GetTestLogic',['../class_test_lib_1_1_testlib2.html#a5dda227e54eed3e9dc09a897de2c9580',1,'TestLib::Testlib2']]],
  ['getuser_263',['GetUser',['../class_logic_1_1_user_logic.html#a14dd43f272d3af129e19c7c6c2a1ee5f',1,'Logic::UserLogic']]],
  ['getusers_264',['GetUsers',['../class_logic_1_1_user_logic.html#acb80fd9de6e3765e3ae0bd21fca4d847',1,'Logic::UserLogic']]]
];
