var searchData=
[
  ['save_112',['Save',['../class_repository_1_1_achievement_repo.html#af94c51bfe8a532654a6bc165c435be2f',1,'Repository.AchievementRepo.Save()'],['../class_repository_1_1_game_repo.html#aef592e8b075f2627fbd66076ada0f35b',1,'Repository.GameRepo.Save()'],['../interface_repository_1_1_i_repo.html#ab9269bcf26891be5ab6162ebfa61d8e8',1,'Repository.IRepo.Save()'],['../class_repository_1_1_user_repo.html#aa077bd71d23cde6917102a082a315026',1,'Repository.UserRepo.Save()']]],
  ['selectedgame_113',['SelectedGame',['../class_user_wpf_1_1_v_m_1_1_main_view_model.html#aeb22c8c6e1ca0f2074c17894a4082268',1,'UserWpf::VM::MainViewModel']]],
  ['setpropertyvalue_114',['SetPropertyValue',['../class_xaml_generated_namespace_1_1_generated_internal_type_helper.html#ade0f04c0f7b18dd5b170e071d5534d38',1,'XamlGeneratedNamespace.GeneratedInternalTypeHelper.SetPropertyValue(System.Reflection.PropertyInfo propertyInfo, object target, object value, System.Globalization.CultureInfo culture)'],['../class_xaml_generated_namespace_1_1_generated_internal_type_helper.html#ade0f04c0f7b18dd5b170e071d5534d38',1,'XamlGeneratedNamespace.GeneratedInternalTypeHelper.SetPropertyValue(System.Reflection.PropertyInfo propertyInfo, object target, object value, System.Globalization.CultureInfo culture)']]],
  ['silver_115',['Silver',['../namespace_models.html#a5885c2ef43f5d49524e50f634d248ebaaf96e345fdc19cbd4cf15256c251a39a0',1,'Models']]],
  ['specificstats_116',['SpecificStats',['../class_feleves__feladat_1_1_controllers_1_1_home_controller.html#a274759a1f757d4d1a8ba299c70f59454',1,'Feleves_feladat::Controllers::HomeController']]],
  ['startup_117',['Startup',['../class_feleves__feladat_1_1_startup.html',1,'Feleves_feladat']]],
  ['stat_118',['Stat',['../class_models_1_1_stat.html',1,'Models']]],
  ['stat2_119',['Stat2',['../class_models_1_1_stat2.html',1,'Models']]],
  ['statistics_120',['Statistics',['../class_feleves__feladat_1_1_controllers_1_1_home_controller.html#ae051f83e3767bbef6c30144323c30915',1,'Feleves_feladat::Controllers::HomeController']]],
  ['sumgametime_121',['SumGameTime',['../class_logic_1_1_game_logic.html#a225b6d583714fbfb21b970eb2d4149be',1,'Logic::GameLogic']]]
];
