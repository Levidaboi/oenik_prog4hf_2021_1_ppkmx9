var annotated_dup =
[
    [ "AspNetCore", "namespace_asp_net_core.html", [
      [ "Views_Home_AddAchievement", "class_asp_net_core_1_1_views___home___add_achievement.html", "class_asp_net_core_1_1_views___home___add_achievement" ],
      [ "Views_Home_AddGame", "class_asp_net_core_1_1_views___home___add_game.html", "class_asp_net_core_1_1_views___home___add_game" ],
      [ "Views_Home_AddUser", "class_asp_net_core_1_1_views___home___add_user.html", "class_asp_net_core_1_1_views___home___add_user" ],
      [ "Views_Home_EditAchi", "class_asp_net_core_1_1_views___home___edit_achi.html", "class_asp_net_core_1_1_views___home___edit_achi" ],
      [ "Views_Home_EditGame", "class_asp_net_core_1_1_views___home___edit_game.html", "class_asp_net_core_1_1_views___home___edit_game" ],
      [ "Views_Home_EditUser", "class_asp_net_core_1_1_views___home___edit_user.html", "class_asp_net_core_1_1_views___home___edit_user" ],
      [ "Views_Home_Index", "class_asp_net_core_1_1_views___home___index.html", "class_asp_net_core_1_1_views___home___index" ],
      [ "Views_Home_ListAchi", "class_asp_net_core_1_1_views___home___list_achi.html", "class_asp_net_core_1_1_views___home___list_achi" ],
      [ "Views_Home_ListAllGames", "class_asp_net_core_1_1_views___home___list_all_games.html", "class_asp_net_core_1_1_views___home___list_all_games" ],
      [ "Views_Home_ListGames", "class_asp_net_core_1_1_views___home___list_games.html", "class_asp_net_core_1_1_views___home___list_games" ],
      [ "Views_Home_ListUsers", "class_asp_net_core_1_1_views___home___list_users.html", "class_asp_net_core_1_1_views___home___list_users" ],
      [ "Views_Home_SpecificStats", "class_asp_net_core_1_1_views___home___specific_stats.html", "class_asp_net_core_1_1_views___home___specific_stats" ],
      [ "Views_Home_Statistics", "class_asp_net_core_1_1_views___home___statistics.html", "class_asp_net_core_1_1_views___home___statistics" ],
      [ "Views_Shared__ValidationScriptsPartial", "class_asp_net_core_1_1_views___shared_____validation_scripts_partial.html", "class_asp_net_core_1_1_views___shared_____validation_scripts_partial" ]
    ] ],
    [ "Data", "namespace_data.html", [
      [ "UserDbContext", "class_data_1_1_user_db_context.html", "class_data_1_1_user_db_context" ]
    ] ],
    [ "Feleves_feladat", "namespace_feleves__feladat.html", [
      [ "Controllers", "namespace_feleves__feladat_1_1_controllers.html", [
        [ "HomeController", "class_feleves__feladat_1_1_controllers_1_1_home_controller.html", "class_feleves__feladat_1_1_controllers_1_1_home_controller" ]
      ] ],
      [ "Program", "class_feleves__feladat_1_1_program.html", "class_feleves__feladat_1_1_program" ],
      [ "Startup", "class_feleves__feladat_1_1_startup.html", "class_feleves__feladat_1_1_startup" ]
    ] ],
    [ "Logic", "namespace_logic.html", [
      [ "AchiLogic", "class_logic_1_1_achi_logic.html", "class_logic_1_1_achi_logic" ],
      [ "GameLogic", "class_logic_1_1_game_logic.html", "class_logic_1_1_game_logic" ],
      [ "UserLogic", "class_logic_1_1_user_logic.html", "class_logic_1_1_user_logic" ]
    ] ],
    [ "Models", "namespace_models.html", [
      [ "Achievement", "class_models_1_1_achievement.html", "class_models_1_1_achievement" ],
      [ "Game", "class_models_1_1_game.html", "class_models_1_1_game" ],
      [ "Stat", "class_models_1_1_stat.html", "class_models_1_1_stat" ],
      [ "Stat2", "class_models_1_1_stat2.html", "class_models_1_1_stat2" ],
      [ "User", "class_models_1_1_user.html", "class_models_1_1_user" ]
    ] ],
    [ "Repository", "namespace_repository.html", [
      [ "AchievementRepo", "class_repository_1_1_achievement_repo.html", "class_repository_1_1_achievement_repo" ],
      [ "GameRepo", "class_repository_1_1_game_repo.html", "class_repository_1_1_game_repo" ],
      [ "IRepo", "interface_repository_1_1_i_repo.html", "interface_repository_1_1_i_repo" ],
      [ "UserRepo", "class_repository_1_1_user_repo.html", "class_repository_1_1_user_repo" ]
    ] ],
    [ "TestLib", "namespace_test_lib.html", [
      [ "Testlib2", "class_test_lib_1_1_testlib2.html", "class_test_lib_1_1_testlib2" ]
    ] ],
    [ "UserWpf", "namespace_user_wpf.html", [
      [ "BL", "namespace_user_wpf_1_1_b_l.html", [
        [ "GameLogic", "class_user_wpf_1_1_b_l_1_1_game_logic.html", "class_user_wpf_1_1_b_l_1_1_game_logic" ],
        [ "IEditorService", "interface_user_wpf_1_1_b_l_1_1_i_editor_service.html", "interface_user_wpf_1_1_b_l_1_1_i_editor_service" ],
        [ "IGameLogic", "interface_user_wpf_1_1_b_l_1_1_i_game_logic.html", "interface_user_wpf_1_1_b_l_1_1_i_game_logic" ]
      ] ],
      [ "Data", "namespace_user_wpf_1_1_data.html", [
        [ "Game", "class_user_wpf_1_1_data_1_1_game.html", "class_user_wpf_1_1_data_1_1_game" ]
      ] ],
      [ "UI", "namespace_user_wpf_1_1_u_i.html", [
        [ "EditorWindow", "class_user_wpf_1_1_u_i_1_1_editor_window.html", "class_user_wpf_1_1_u_i_1_1_editor_window" ],
        [ "Window1", "class_user_wpf_1_1_u_i_1_1_window1.html", "class_user_wpf_1_1_u_i_1_1_window1" ],
        [ "EditorServiceViaWindow", "class_user_wpf_1_1_u_i_1_1_editor_service_via_window.html", "class_user_wpf_1_1_u_i_1_1_editor_service_via_window" ]
      ] ],
      [ "VM", "namespace_user_wpf_1_1_v_m.html", [
        [ "EditorViewModel", "class_user_wpf_1_1_v_m_1_1_editor_view_model.html", "class_user_wpf_1_1_v_m_1_1_editor_view_model" ],
        [ "MainViewModel", "class_user_wpf_1_1_v_m_1_1_main_view_model.html", "class_user_wpf_1_1_v_m_1_1_main_view_model" ]
      ] ],
      [ "App", "class_user_wpf_1_1_app.html", "class_user_wpf_1_1_app" ],
      [ "MainWindow", "class_user_wpf_1_1_main_window.html", "class_user_wpf_1_1_main_window" ],
      [ "MyIoc", "class_user_wpf_1_1_my_ioc.html", "class_user_wpf_1_1_my_ioc" ]
    ] ],
    [ "XamlGeneratedNamespace", "namespace_xaml_generated_namespace.html", [
      [ "GeneratedInternalTypeHelper", "class_xaml_generated_namespace_1_1_generated_internal_type_helper.html", "class_xaml_generated_namespace_1_1_generated_internal_type_helper" ]
    ] ]
];