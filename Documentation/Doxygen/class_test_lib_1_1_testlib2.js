var class_test_lib_1_1_testlib2 =
[
    [ "AddUser", "class_test_lib_1_1_testlib2.html#a1f775d7ac0c7034c10888f673e425029", null ],
    [ "DeleteAchi", "class_test_lib_1_1_testlib2.html#ae9a2a21629c809a627c2e4b54378e163", null ],
    [ "EditAchi", "class_test_lib_1_1_testlib2.html#aef50e060ed296766b0f1bfc91c4a5ba1", null ],
    [ "GetAGame", "class_test_lib_1_1_testlib2.html#a49933106173dfd05f2c9cbf4c4eae421", null ],
    [ "GetAllUsers", "class_test_lib_1_1_testlib2.html#ae92d48d13360cb49eed7fc39ccd7c748", null ],
    [ "GetBestGamer", "class_test_lib_1_1_testlib2.html#aced6cf982130aca334eea1d27af9a381", null ],
    [ "GetLifelessGamer", "class_test_lib_1_1_testlib2.html#ae8668ee115c92614f28e5340b5e76feb", null ],
    [ "GetPickiestGamer", "class_test_lib_1_1_testlib2.html#a73d1724626d37125700d240257d2459d", null ],
    [ "GetTestLogic", "class_test_lib_1_1_testlib2.html#a5dda227e54eed3e9dc09a897de2c9580", null ]
];