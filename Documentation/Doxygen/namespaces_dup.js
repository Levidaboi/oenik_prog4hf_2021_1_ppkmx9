var namespaces_dup =
[
    [ "AspNetCore", "namespace_asp_net_core.html", "namespace_asp_net_core" ],
    [ "Data", "namespace_data.html", "namespace_data" ],
    [ "Feleves_feladat", "namespace_feleves__feladat.html", "namespace_feleves__feladat" ],
    [ "Logic", "namespace_logic.html", "namespace_logic" ],
    [ "Models", "namespace_models.html", "namespace_models" ],
    [ "Repository", "namespace_repository.html", "namespace_repository" ],
    [ "TestLib", "namespace_test_lib.html", "namespace_test_lib" ],
    [ "UserWpf", "namespace_user_wpf.html", "namespace_user_wpf" ],
    [ "XamlGeneratedNamespace", "namespace_xaml_generated_namespace.html", "namespace_xaml_generated_namespace" ]
];