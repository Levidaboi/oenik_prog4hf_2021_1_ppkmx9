var namespace_models =
[
    [ "Achievement", "class_models_1_1_achievement.html", "class_models_1_1_achievement" ],
    [ "Game", "class_models_1_1_game.html", "class_models_1_1_game" ],
    [ "Stat", "class_models_1_1_stat.html", "class_models_1_1_stat" ],
    [ "Stat2", "class_models_1_1_stat2.html", "class_models_1_1_stat2" ],
    [ "User", "class_models_1_1_user.html", "class_models_1_1_user" ],
    [ "AchiLevel", "namespace_models.html#a5885c2ef43f5d49524e50f634d248eba", [
      [ "Bronze", "namespace_models.html#a5885c2ef43f5d49524e50f634d248ebaaa0df72576daf9a1137d39f7553d36e82", null ],
      [ "Silver", "namespace_models.html#a5885c2ef43f5d49524e50f634d248ebaaf96e345fdc19cbd4cf15256c251a39a0", null ],
      [ "Gold", "namespace_models.html#a5885c2ef43f5d49524e50f634d248ebaa9768feb3fdb1f267b06093bc572952dd", null ]
    ] ]
];