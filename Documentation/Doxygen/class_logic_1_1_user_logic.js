var class_logic_1_1_user_logic =
[
    [ "UserLogic", "class_logic_1_1_user_logic.html#a0a0b530f3b049638c99e3e759432f64a", null ],
    [ "UserLogic", "class_logic_1_1_user_logic.html#a2a2932cd83bcabb0f8d61d42b915a147", null ],
    [ "UserLogic", "class_logic_1_1_user_logic.html#af2b8e9f76a728dc72fded891bb511080", null ],
    [ "AddNewUser", "class_logic_1_1_user_logic.html#aeb74edb73f5321c42e4c6f9e0d25457a", null ],
    [ "DeleteUser", "class_logic_1_1_user_logic.html#af5718972e2970f4825a0769213f88248", null ],
    [ "GetAchiPoints", "class_logic_1_1_user_logic.html#a9b94493d362273fed80399cf673036b0", null ],
    [ "GetBestGamer", "class_logic_1_1_user_logic.html#acd96baac5b91942ecbc522895f559ae3", null ],
    [ "GetGameCount", "class_logic_1_1_user_logic.html#a60fe6731054146b991771724212f954f", null ],
    [ "GetGameTime", "class_logic_1_1_user_logic.html#aa5c18f79edc514e3f7e0a75d18c2547a", null ],
    [ "GetLifelessGamer", "class_logic_1_1_user_logic.html#acead56c5846eb6aec52db33a5c35eb9b", null ],
    [ "GetPickiestGamer", "class_logic_1_1_user_logic.html#a7d222b65ba55fed97cb9e9bcc6c60a83", null ],
    [ "GetRichestGamer", "class_logic_1_1_user_logic.html#a50d9f267de1f5fd88b0a137af3749d08", null ],
    [ "GetUser", "class_logic_1_1_user_logic.html#a14dd43f272d3af129e19c7c6c2a1ee5f", null ],
    [ "GetUsers", "class_logic_1_1_user_logic.html#acb80fd9de6e3765e3ae0bd21fca4d847", null ],
    [ "UpdateUser", "class_logic_1_1_user_logic.html#ad262a18d4c2a4a5a8a79958f655a8ff5", null ]
];