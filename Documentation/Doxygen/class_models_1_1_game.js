var class_models_1_1_game =
[
    [ "Equals", "class_models_1_1_game.html#a73473144c2d9fc1e04fb55277cce0302", null ],
    [ "GetHashCode", "class_models_1_1_game.html#a8274ffdfa57ca0d7c54e0efc6427755f", null ],
    [ "Achievements", "class_models_1_1_game.html#a9d03be2ec0916df8d060c4afedca0a02", null ],
    [ "GameId", "class_models_1_1_game.html#ac4cccb284483f5536bbe4d4ce24d5b8a", null ],
    [ "GameTime", "class_models_1_1_game.html#adca976481870a71a98e585ff6be2598f", null ],
    [ "Genre", "class_models_1_1_game.html#aae7cc6d571c73566ec8bb973bc92b5ed", null ],
    [ "Name", "class_models_1_1_game.html#a42be168b7bd794c2b92e9ae177e7fe62", null ],
    [ "PlayerName", "class_models_1_1_game.html#a4390a95eaeab63cd9ca997c4b099e346", null ],
    [ "Rating", "class_models_1_1_game.html#a0ee59b01879590a2de43209587b7d019", null ],
    [ "User", "class_models_1_1_game.html#adfff3ed4309b6f4b8f7a60655026c22e", null ],
    [ "UserId", "class_models_1_1_game.html#ab44bc9d7fcade8586799fabaf2e8d5b1", null ]
];