var hierarchy =
[
    [ "Models.Achievement", "class_models_1_1_achievement.html", null ],
    [ "Logic.AchiLogic", "class_logic_1_1_achi_logic.html", null ],
    [ "Application", null, [
      [ "UserWpf.App", "class_user_wpf_1_1_app.html", null ]
    ] ],
    [ "System.Windows.Application", null, [
      [ "UserWpf.App", "class_user_wpf_1_1_app.html", null ],
      [ "UserWpf.App", "class_user_wpf_1_1_app.html", null ],
      [ "UserWpf.App", "class_user_wpf_1_1_app.html", null ],
      [ "UserWpf.App", "class_user_wpf_1_1_app.html", null ],
      [ "UserWpf.App", "class_user_wpf_1_1_app.html", null ]
    ] ],
    [ "Controller", null, [
      [ "Feleves_feladat.Controllers.HomeController", "class_feleves__feladat_1_1_controllers_1_1_home_controller.html", null ]
    ] ],
    [ "DbContext", null, [
      [ "Data.UserDbContext", "class_data_1_1_user_db_context.html", null ]
    ] ],
    [ "Models.Game", "class_models_1_1_game.html", null ],
    [ "Logic.GameLogic", "class_logic_1_1_game_logic.html", null ],
    [ "System.Windows.Markup.IComponentConnector", null, [
      [ "UserWpf.MainWindow", "class_user_wpf_1_1_main_window.html", null ],
      [ "UserWpf.MainWindow", "class_user_wpf_1_1_main_window.html", null ],
      [ "UserWpf.MainWindow", "class_user_wpf_1_1_main_window.html", null ],
      [ "UserWpf.MainWindow", "class_user_wpf_1_1_main_window.html", null ],
      [ "UserWpf.MainWindow", "class_user_wpf_1_1_main_window.html", null ],
      [ "UserWpf.UI.EditorWindow", "class_user_wpf_1_1_u_i_1_1_editor_window.html", null ],
      [ "UserWpf.UI.EditorWindow", "class_user_wpf_1_1_u_i_1_1_editor_window.html", null ],
      [ "UserWpf.UI.EditorWindow", "class_user_wpf_1_1_u_i_1_1_editor_window.html", null ],
      [ "UserWpf.UI.Window1", "class_user_wpf_1_1_u_i_1_1_window1.html", null ],
      [ "UserWpf.UI.Window1", "class_user_wpf_1_1_u_i_1_1_window1.html", null ]
    ] ],
    [ "UserWpf.BL.IEditorService", "interface_user_wpf_1_1_b_l_1_1_i_editor_service.html", [
      [ "UserWpf.UI.EditorServiceViaWindow", "class_user_wpf_1_1_u_i_1_1_editor_service_via_window.html", null ]
    ] ],
    [ "UserWpf.BL.IGameLogic", "interface_user_wpf_1_1_b_l_1_1_i_game_logic.html", [
      [ "UserWpf.BL.GameLogic", "class_user_wpf_1_1_b_l_1_1_game_logic.html", null ]
    ] ],
    [ "System.Windows.Markup.InternalTypeHelper", null, [
      [ "XamlGeneratedNamespace.GeneratedInternalTypeHelper", "class_xaml_generated_namespace_1_1_generated_internal_type_helper.html", null ],
      [ "XamlGeneratedNamespace.GeneratedInternalTypeHelper", "class_xaml_generated_namespace_1_1_generated_internal_type_helper.html", null ]
    ] ],
    [ "Repository.IRepo< T >", "interface_repository_1_1_i_repo.html", null ],
    [ "Repository.IRepo< Achievement >", "interface_repository_1_1_i_repo.html", [
      [ "Repository.AchievementRepo", "class_repository_1_1_achievement_repo.html", null ]
    ] ],
    [ "Repository.IRepo< Game >", "interface_repository_1_1_i_repo.html", [
      [ "Repository.GameRepo", "class_repository_1_1_game_repo.html", null ]
    ] ],
    [ "Repository.IRepo< Models.Achievement >", "interface_repository_1_1_i_repo.html", null ],
    [ "Repository.IRepo< Models.Game >", "interface_repository_1_1_i_repo.html", null ],
    [ "Repository.IRepo< Models.User >", "interface_repository_1_1_i_repo.html", null ],
    [ "Repository.IRepo< User >", "interface_repository_1_1_i_repo.html", [
      [ "Repository.UserRepo", "class_repository_1_1_user_repo.html", null ]
    ] ],
    [ "IServiceLocator", null, [
      [ "UserWpf.MyIoc", "class_user_wpf_1_1_my_ioc.html", null ]
    ] ],
    [ "ObservableObject", null, [
      [ "UserWpf.Data.Game", "class_user_wpf_1_1_data_1_1_game.html", null ]
    ] ],
    [ "Feleves_feladat.Program", "class_feleves__feladat_1_1_program.html", null ],
    [ "global.MicrosoftAspNetCore.Mvc.Razor.RazorPage", null, [
      [ "AspNetCore.Views_Home_AddAchievement", "class_asp_net_core_1_1_views___home___add_achievement.html", null ],
      [ "AspNetCore.Views_Home_AddGame", "class_asp_net_core_1_1_views___home___add_game.html", null ],
      [ "AspNetCore.Views_Home_AddUser", "class_asp_net_core_1_1_views___home___add_user.html", null ],
      [ "AspNetCore.Views_Home_EditAchi", "class_asp_net_core_1_1_views___home___edit_achi.html", null ],
      [ "AspNetCore.Views_Home_EditGame", "class_asp_net_core_1_1_views___home___edit_game.html", null ],
      [ "AspNetCore.Views_Home_EditUser", "class_asp_net_core_1_1_views___home___edit_user.html", null ],
      [ "AspNetCore.Views_Home_Index", "class_asp_net_core_1_1_views___home___index.html", null ],
      [ "AspNetCore.Views_Home_SpecificStats", "class_asp_net_core_1_1_views___home___specific_stats.html", null ],
      [ "AspNetCore.Views_Shared__ValidationScriptsPartial", "class_asp_net_core_1_1_views___shared_____validation_scripts_partial.html", null ]
    ] ],
    [ "global.MicrosoftAspNetCore.Mvc.Razor.RazorPage< IEnumerable< Achievement >>", null, [
      [ "AspNetCore.Views_Home_ListAchi", "class_asp_net_core_1_1_views___home___list_achi.html", null ]
    ] ],
    [ "global.MicrosoftAspNetCore.Mvc.Razor.RazorPage< IEnumerable< Game >>", null, [
      [ "AspNetCore.Views_Home_ListAllGames", "class_asp_net_core_1_1_views___home___list_all_games.html", null ],
      [ "AspNetCore.Views_Home_ListGames", "class_asp_net_core_1_1_views___home___list_games.html", null ]
    ] ],
    [ "global.MicrosoftAspNetCore.Mvc.Razor.RazorPage< IEnumerable< Models.Stat >>", null, [
      [ "AspNetCore.Views_Home_Statistics", "class_asp_net_core_1_1_views___home___statistics.html", null ]
    ] ],
    [ "global.MicrosoftAspNetCore.Mvc.Razor.RazorPage< IEnumerable< User >>", null, [
      [ "AspNetCore.Views_Home_ListUsers", "class_asp_net_core_1_1_views___home___list_users.html", null ]
    ] ],
    [ "SimpleIoc", null, [
      [ "UserWpf.MyIoc", "class_user_wpf_1_1_my_ioc.html", null ]
    ] ],
    [ "Feleves_feladat.Startup", "class_feleves__feladat_1_1_startup.html", null ],
    [ "Models.Stat", "class_models_1_1_stat.html", null ],
    [ "Models.Stat2", "class_models_1_1_stat2.html", null ],
    [ "TestLib.Testlib2", "class_test_lib_1_1_testlib2.html", null ],
    [ "Models.User", "class_models_1_1_user.html", null ],
    [ "Logic.UserLogic", "class_logic_1_1_user_logic.html", null ],
    [ "ViewModelBase", null, [
      [ "UserWpf.VM.EditorViewModel", "class_user_wpf_1_1_v_m_1_1_editor_view_model.html", null ],
      [ "UserWpf.VM.MainViewModel", "class_user_wpf_1_1_v_m_1_1_main_view_model.html", null ]
    ] ],
    [ "System.Windows.Window", null, [
      [ "UserWpf.MainWindow", "class_user_wpf_1_1_main_window.html", null ],
      [ "UserWpf.MainWindow", "class_user_wpf_1_1_main_window.html", null ],
      [ "UserWpf.MainWindow", "class_user_wpf_1_1_main_window.html", null ],
      [ "UserWpf.MainWindow", "class_user_wpf_1_1_main_window.html", null ],
      [ "UserWpf.MainWindow", "class_user_wpf_1_1_main_window.html", null ],
      [ "UserWpf.UI.EditorWindow", "class_user_wpf_1_1_u_i_1_1_editor_window.html", null ],
      [ "UserWpf.UI.EditorWindow", "class_user_wpf_1_1_u_i_1_1_editor_window.html", null ],
      [ "UserWpf.UI.EditorWindow", "class_user_wpf_1_1_u_i_1_1_editor_window.html", null ],
      [ "UserWpf.UI.EditorWindow", "class_user_wpf_1_1_u_i_1_1_editor_window.html", null ],
      [ "UserWpf.UI.Window1", "class_user_wpf_1_1_u_i_1_1_window1.html", null ],
      [ "UserWpf.UI.Window1", "class_user_wpf_1_1_u_i_1_1_window1.html", null ]
    ] ],
    [ "Window", null, [
      [ "UserWpf.MainWindow", "class_user_wpf_1_1_main_window.html", null ]
    ] ]
];