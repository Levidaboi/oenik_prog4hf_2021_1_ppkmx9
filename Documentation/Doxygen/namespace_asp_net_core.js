var namespace_asp_net_core =
[
    [ "Views_Home_AddAchievement", "class_asp_net_core_1_1_views___home___add_achievement.html", "class_asp_net_core_1_1_views___home___add_achievement" ],
    [ "Views_Home_AddGame", "class_asp_net_core_1_1_views___home___add_game.html", "class_asp_net_core_1_1_views___home___add_game" ],
    [ "Views_Home_AddUser", "class_asp_net_core_1_1_views___home___add_user.html", "class_asp_net_core_1_1_views___home___add_user" ],
    [ "Views_Home_EditAchi", "class_asp_net_core_1_1_views___home___edit_achi.html", "class_asp_net_core_1_1_views___home___edit_achi" ],
    [ "Views_Home_EditGame", "class_asp_net_core_1_1_views___home___edit_game.html", "class_asp_net_core_1_1_views___home___edit_game" ],
    [ "Views_Home_EditUser", "class_asp_net_core_1_1_views___home___edit_user.html", "class_asp_net_core_1_1_views___home___edit_user" ],
    [ "Views_Home_Index", "class_asp_net_core_1_1_views___home___index.html", "class_asp_net_core_1_1_views___home___index" ],
    [ "Views_Home_ListAchi", "class_asp_net_core_1_1_views___home___list_achi.html", "class_asp_net_core_1_1_views___home___list_achi" ],
    [ "Views_Home_ListAllGames", "class_asp_net_core_1_1_views___home___list_all_games.html", "class_asp_net_core_1_1_views___home___list_all_games" ],
    [ "Views_Home_ListGames", "class_asp_net_core_1_1_views___home___list_games.html", "class_asp_net_core_1_1_views___home___list_games" ],
    [ "Views_Home_ListUsers", "class_asp_net_core_1_1_views___home___list_users.html", "class_asp_net_core_1_1_views___home___list_users" ],
    [ "Views_Home_SpecificStats", "class_asp_net_core_1_1_views___home___specific_stats.html", "class_asp_net_core_1_1_views___home___specific_stats" ],
    [ "Views_Home_Statistics", "class_asp_net_core_1_1_views___home___statistics.html", "class_asp_net_core_1_1_views___home___statistics" ],
    [ "Views_Shared__ValidationScriptsPartial", "class_asp_net_core_1_1_views___shared_____validation_scripts_partial.html", "class_asp_net_core_1_1_views___shared_____validation_scripts_partial" ]
];