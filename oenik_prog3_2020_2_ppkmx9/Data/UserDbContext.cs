﻿// <copyright file="UserDbContext.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

[assembly: System.CLSCompliant(false)]

namespace Data
{
    using Microsoft.EntityFrameworkCore;
    using Models;

    /// <summary>
    /// Documentation of the public UserDbContext class.
    /// </summary>
    public class UserDbContext : DbContext
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="UserDbContext"/> class.
        /// </summary>
        /// <param name="opt">the first parameter of the constuctor.</param>
        public UserDbContext(DbContextOptions<UserDbContext> opt)
            : base(opt)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="UserDbContext"/> class.
        /// </summary>
        public UserDbContext()
        {
            this.Database.EnsureCreated();
        }

        /// <summary>
        /// Gets or sets documentation for the Users.
        /// </summary>
        public DbSet<User> Users { get; set; }

        /// <summary>
        /// Gets or sets documentation for the Games.
        /// </summary>
        public DbSet<Game> Games { get; set; }

        /// <summary>
        /// Gets or sets documentation for the Achievements.
        /// </summary>
        public DbSet<Achievement> Achievements { get; set; }

        /// <inheritdoc/>
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.
                    UseLazyLoadingProxies().
                    UseSqlServer(@"data source=(LocalDB)\MSSQLLocalDB;attachdbfilename=|DataDirectory|\UserDb.mdf;integrated security=True;MultipleActiveResultSets=True");
            }
        }

        /// <inheritdoc/>
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Achievement>(entity =>
            {
                entity
                .HasOne(achievement => achievement.Game)
                .WithMany(game => game.Achievements)
                .HasForeignKey(achievement => achievement.GameId)
                .OnDelete(DeleteBehavior.Cascade);
            });

            modelBuilder.Entity<Game>(entity =>
            {
                entity
                .HasOne(game => game.User)
                .WithMany(user => user.GameLibrary)
                .HasForeignKey(game => game.UserId)
                .OnDelete(DeleteBehavior.Cascade);
            });
        }
    }
}