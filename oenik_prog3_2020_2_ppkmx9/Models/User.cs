﻿// <copyright file="User.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Models
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    /// <summary>
    /// Documentation of the public User class.
    /// </summary>
    public class User
    {
        /// <summary>
        /// Gets or sets documentation for the UserId.
        /// </summary>
        [Key]
        public string UserId { get; set; }

        /// <summary>
        /// Gets or sets documentation for the Name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets documentation for the Age.
        /// </summary>
        public int Age { get; set; }

        /// <summary>
        /// Gets or sets documentation for the GameLibrary.
        /// </summary>
        public virtual ICollection<Game> GameLibrary { get; set; }

        /// <inheritdoc/>
        public override bool Equals(object obj)
        {
            if (obj is User)
            {
                User u = obj as User;
                return this.UserId == u.UserId && this.Name == u.Name &&
                    this.Age == u.Age && this.GameLibrary == u.GameLibrary;
            }
            else
            {
                return false;
            }
        }

        /// <inheritdoc/>
        public override int GetHashCode()
        {
            return 0;
        }
    }
}