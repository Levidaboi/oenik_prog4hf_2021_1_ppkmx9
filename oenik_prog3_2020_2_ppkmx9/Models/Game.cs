﻿// <copyright file="Game.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    /// <summary>
    /// Documentation of the public Game class.
    /// </summary>
    public class Game
    {
        /// <summary>
        /// Gets or sets documentation for the GameId.
        /// </summary>
        [Key]
        public string GameId { get; set; }

        /// <summary>
        /// Gets or sets documentation for the Name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets documentation for the Genre.
        /// </summary>
        public string Genre { get; set; }

        /// <summary>
        /// Gets or sets documentation for the GameTime.
        /// </summary>
        public int GameTime { get; set; }

        /// <summary>
        /// Gets or sets documentation for the PlayerName.
        /// </summary>
        public string PlayerName { get; set; }

        /// <summary>
        /// Gets or sets documentation for the Rating.
        /// </summary>
        [Range(0.0, 10.0)]
        public int Rating { get; set; }

        /// <summary>
        /// Gets or sets documentation for the UserId.
        /// </summary>
        public string UserId { get; set; }

        /// <summary>
        /// Gets or sets documentation for the User.
        /// </summary>
        [NotMapped]
        public virtual User User { get; set; }

        /// <summary>
        /// Gets or sets documentation for the PlayerName.
        /// </summary>
        public virtual ICollection<Achievement> Achievements { get; set; }

        /// <inheritdoc/>
        public override bool Equals(object obj)
        {
            if (obj is Game)
            {
                Game u = obj as Game;
                return this.UserId == u.UserId && this.Name == u.Name &&
                    this.Genre == u.Genre && this.Achievements == u.Achievements
                    && this.GameTime == u.GameTime && this.GameId == u.GameId &&
                    this.Rating == u.Rating;
            }
            else
            {
                return false;
            }
        }

        /// <inheritdoc/>
        public override int GetHashCode()
        {
            return 0;
        }
    }
}