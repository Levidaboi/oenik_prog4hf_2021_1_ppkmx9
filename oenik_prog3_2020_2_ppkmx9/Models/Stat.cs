﻿// <copyright file="Stat.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Models
{
    /// <summary>
    /// Documentation of the public Stat class.
    /// </summary>
    public class Stat
    {
        /// <summary>
        /// Gets or sets documentation for the PlayerName.
        /// </summary>
        public string PlayerName { get; set; }

        /// <summary>
        /// Gets or sets documentation for the GameCount.
        /// </summary>
        public int GameCount { get; set; }

        /// <summary>
        /// Gets or sets documentation for the GameTime.
        /// </summary>
        public int GameTime { get; set; }

        /// <summary>
        /// Gets or sets documentation for the AchiPoints.
        /// </summary>
        public int AchiPoints { get; set; }
    }
}