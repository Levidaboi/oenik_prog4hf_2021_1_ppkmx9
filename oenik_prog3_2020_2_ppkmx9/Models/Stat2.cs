﻿// <copyright file="Stat2.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Models
{
    /// <summary>
    /// Documentation of the public Stat2 class.
    /// </summary>
    public class Stat2
    {
        /// <summary>
        /// Gets or sets documentation for the Users.
        /// </summary>
        public string BestGamer { get; set; }

        /// <summary>
        /// Gets or sets documentation for the RichestGamer.
        /// </summary>
        public string RichestGamer { get; set; }

        /// <summary>
        /// Gets or sets documentation for the LifelessGamer.
        /// </summary>
        public string LifelessGamer { get; set; }

        /// <summary>
        /// Gets or sets documentation for the PickiestGamer.
        /// </summary>
        public string PickiestGamer { get; set; }
    }
}