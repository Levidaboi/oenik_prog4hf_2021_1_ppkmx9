﻿// <copyright file="Achievement.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

[assembly: System.CLSCompliant(false)]

namespace Models
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    /// <summary>
    /// Documentation of the public enum AchiLevel.
    /// </summary>
    public enum AchiLevel
    {
        /// <summary>
        /// Documentation of the enum item Bronze.
        /// </summary>
        Bronze = 10,

        /// <summary>
        /// Documentation of the enum item Silver.
        /// </summary>
        Silver = 20,

        /// <summary>
        /// Documentation of the enum item Gold.
        /// </summary>
        Gold = 30,
    }

    /// <summary>
    /// Documentation of the public Achievement class.
    /// </summary>
    public class Achievement
    {
        /// <summary>
        /// Gets or sets documentation for the AchiId.
        /// </summary>
        [Key]
        public string AchiId { get; set; }

        /// <summary>
        /// Gets or sets documentation for the Name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets documentation for the AchiLevel.
        /// </summary>
        public AchiLevel AchiLevel { get; set; }

        /// <summary>
        /// Gets or sets documentation for the GameId.
        /// </summary>
        public string GameId { get; set; }

        /// <summary>
        /// Gets or sets documentation for the Game.
        /// </summary>
        [NotMapped]
        public virtual Game Game { get; set; }

        /// <inheritdoc/>
        public override bool Equals(object obj)
        {
            if (obj is Achievement)
            {
                Achievement u = obj as Achievement;
                return this.AchiId == u.AchiId && this.Name == u.Name &&
                    this.AchiLevel == u.AchiLevel && this.GameId == u.GameId
                    && this.Game == u.Game;
            }
            else
            {
                return false;
            }
        }

        /// <inheritdoc/>
        public override int GetHashCode()
        {
            return 0;
        }
    }
}