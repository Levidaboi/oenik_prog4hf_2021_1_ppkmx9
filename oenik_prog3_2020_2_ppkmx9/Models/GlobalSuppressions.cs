﻿// <copyright file="GlobalSuppressions.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage("Usage", "CA2227:Collection properties should be read only", Justification = "useless", Scope = "member", Target = "~P:Models.User.GameLibrary")]
[assembly: SuppressMessage("Design", "CA1008:Enums should have zero value", Justification = "useless", Scope = "type", Target = "~T:Models.AchiLevel")]
[assembly: SuppressMessage("StyleCop.CSharp.MaintainabilityRules", "SA1404:Code analysis suppression should have justification", Justification = "useless")]
[assembly: SuppressMessage("Usage", "CA2227:Collection properties should be read only", Justification = "useless", Scope = "member", Target = "~P:Models.Game.Achievements")]
