﻿// <copyright file="Game.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace UserWpf.Data
{
    using System.Linq;
    using GalaSoft.MvvmLight;

    /// <summary>
    /// Documentation of the public Game.
    /// </summary>
    public class Game : ObservableObject
    {
        private string gameId;
        private string name;
        private string genre;
        private int gameTime;
        private int rating;

        /// <summary>
        /// Gets or sets documentation for the GameId.
        /// </summary>
        public string GameId
        {
            get { return this.gameId; }
            set { this.Set(ref this.gameId, value); }
        }

        /// <summary>
        /// Gets or sets documentation for the Name.
        /// </summary>
        public string Name
        {
            get { return this.name; }
            set { this.Set(ref this.name, value); }
        }

        /// <summary>
        /// Gets or sets documentation for the Genre.
        /// </summary>
        public string Genre
        {
            get { return this.genre; }
            set { this.Set(ref this.genre, value); }
        }

        /// <summary>
        /// Gets or sets documentation for the GameTime.
        /// </summary>
        public int GameTime
        {
            get { return this.gameTime; }
            set { this.Set(ref this.gameTime, value); }
        }

        /// <summary>
        /// Gets or sets documentation for the Rating.
        /// </summary>
        public int Rating
        {
            get { return this.rating; }
            set { this.Set(ref this.rating, value); }
        }

        /// <summary>
        /// copies the properties from the parameter.
        /// </summary>
        /// <param name="other">the first parameter of the method.</param>
        public void CopyFrom(Game other)
        {
            this.GetType().GetProperties().ToList().ForEach(
                property => property.SetValue(this, property.GetValue(other)));
        }
    }
}