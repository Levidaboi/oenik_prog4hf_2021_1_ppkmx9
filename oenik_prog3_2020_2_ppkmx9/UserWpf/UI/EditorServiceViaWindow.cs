﻿// <copyright file="EditorServiceViaWindow.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace UserWpf.UI
{
    using UserWpf.BL;
    using UserWpf.Data;

    /// <summary>
    /// Documentation of the internal EditorServiceViaWindow.
    /// </summary>
    internal class EditorServiceViaWindow : IEditorService
    {
        /// <summary>
        /// edits a game.
        /// </summary>
        /// <param name="p">the first parameter of the method.</param>
        /// <returns> all games .</returns>
        public bool EditGame(Game p)
        {
            EditorWindow win = new EditorWindow(p);
            return win.ShowDialog() ?? false;
        }
    }
}