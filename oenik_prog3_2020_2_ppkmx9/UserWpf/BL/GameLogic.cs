﻿[assembly: System.CLSCompliant(false)]

namespace UserWpf.BL
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using GalaSoft.MvvmLight.Messaging;
    using UserWpf.Data;

    /// <summary>
    /// Documentation of the internal GameLogic.
    /// </summary>
    internal class GameLogic : IGameLogic
    {
        private IEditorService editorService;
        private IMessenger messengerService;
        private Logic.GameLogic gameLogic;

        /// <summary>
        /// Initializes a new instance of the <see cref="GameLogic"/> class.
        /// </summary>
        /// <param name="editor">the first parameter of the constuctor.</param>
        /// /// <param name="messengerService">the second parameter of the constuctor.</param>
        /// /// <param name="gameLogic">the third parameter of the constuctor.</param>
        public GameLogic(IEditorService editor, IMessenger messengerService, Logic.GameLogic gameLogic)
        {
            this.gameLogic = gameLogic;
            this.editorService = editor;
            this.messengerService = messengerService;
        }

        /// <summary>
        /// adds a game.
        /// </summary>
        /// <param name="list">the first parameter of the method.</param>
        public void AddGame(IList<Game> list)
        {
            Game newGame = new Game();
            if (this.editorService.EditGame(newGame) == true)
            {
                newGame.GameId = Guid.NewGuid().ToString();
                list.Add(newGame);
                Models.Game game = new Models.Game();
                game.GameId = newGame.GameId;
                game.Genre = newGame.Genre;
                game.GameTime = newGame.GameTime;
                game.Name = newGame.Name;
                game.Rating = newGame.Rating;
                game.GameId = newGame.GameId;
                this.gameLogic.AddGame(game);
                this.messengerService.Send("ADD OK", "LogicResult");
            }
            else
            {
                this.messengerService.Send("ADD CANCEL", "LogicResult");
            }
        }

        /// <summary>
        /// deletes a game.
        /// </summary>
        /// <param name="list">the first parameter of the method.</param>
        /// <param name="game">the second parameter of the method.</param>
        public void DelGame(IList<Game> list, Game game)
        {
            if (game != null && list.Remove(game))
            {
                this.gameLogic.DeleteGame(game.GameId);

                this.messengerService.Send("DELETE OK", "LogicResult");
            }
            else
            {
                this.messengerService.Send("DELETE FAILED", "LogicResult");
            }
        }

        /// <summary>
        /// gets all games.
        /// </summary>
        /// <returns> all games .</returns>
        public IList<Game> GetAllGames()
        {
            List<Models.Game> games = this.gameLogic.GetAllGames().ToList();

            List<UserWpf.Data.Game> gs = new List<Game>();
            foreach (var item in games)
            {
                UserWpf.Data.Game g = new Game();
                g.GameId = item.GameId;
                g.GameTime = item.GameTime;
                g.Genre = item.Genre;
                g.Name = item.Name;
                g.Rating = item.Rating;

                gs.Add(g);
            }

            return gs;
        }

        /// <summary>
        /// modifies a game.
        /// </summary>
        /// <param name="gameToModify">the first parameter of the method.</param>
        public void ModGame(Game gameToModify)
        {
            if (gameToModify == null)
            {
                this.messengerService.Send("EDIT FAILED", "LogicResult");
                return;
            }

            Game clone = new Game();
            clone.CopyFrom(gameToModify);
            if (this.editorService.EditGame(clone) == true)
            {
                gameToModify.CopyFrom(clone);

                Models.Game game = new Models.Game();
                game.GameId = gameToModify.GameId;
                game.Genre = gameToModify.Genre;
                game.GameTime = gameToModify.GameTime;
                game.Name = gameToModify.Name;
                game.Rating = gameToModify.Rating;

                this.gameLogic.UpdateGame(game.GameId, game);
                this.messengerService.Send("MODIFY OK", "LogicResult");
            }
            else
            {
                this.messengerService.Send("MODIFY CANCEL", "LogicResult");
            }
        }
    }
}