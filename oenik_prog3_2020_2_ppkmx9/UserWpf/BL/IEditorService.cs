﻿// <copyright file="IEditorService.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace UserWpf.BL
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using UserWpf.Data;

    /// <summary>
    /// Documentation of the internal IEditorService.
    /// </summary>
    public interface IEditorService
    {
        /// <summary>
        /// Documentation of the internal IEditorService.
        /// </summary>
        /// <param name="p">the first parameter of the method.</param>
        /// /// <returns> if the methos was succesful .</returns>
        bool EditGame(Game p);
    }
}
