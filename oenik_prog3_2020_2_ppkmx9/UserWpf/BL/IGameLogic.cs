﻿// <copyright file="IGameLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace UserWpf.BL
{
    using System.Collections.Generic;
    using UserWpf.Data;

    /// <summary>
    /// Documentation of the internal IGameLogic.
    /// </summary>
    internal interface IGameLogic
    {
        /// <summary>
        /// adds a game.
        /// </summary>
        /// <param name="list">the first parameter of the method.</param>
        void AddGame(IList<Game> list);

        /// <summary>
        /// modifies a game.
        /// </summary>
        /// <param name="gameToModify">the first parameter of the method.</param>
        void ModGame(Game gameToModify);

        /// <summary>
        /// deletes a game.
        /// </summary>
        /// <param name="list">the first parameter of the method.</param>
        /// <param name="game">the second parameter of the method.</param>
        void DelGame(IList<Game> list, Game game);

        /// <summary>
        /// gets all games.
        /// </summary>
        /// <returns> all games .</returns>
        IList<Game> GetAllGames();
    }
}