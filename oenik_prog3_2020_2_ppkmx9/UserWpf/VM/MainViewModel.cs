﻿// <copyright file="MainViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace UserWpf.VM
{
    using System.Collections.ObjectModel;
    using System.Windows.Input;
    using CommonServiceLocator;
    using GalaSoft.MvvmLight;
    using GalaSoft.MvvmLight.Command;
    using UserWpf.BL;
    using UserWpf.Data;

    /// <summary>
    /// Documentation of the internal MainViewModel.
    /// </summary>
    internal class MainViewModel : ViewModelBase
    {
        private IGameLogic logic;

        private Game selectedGame;

        /// <summary>
        /// Initializes a new instance of the <see cref="MainViewModel"/> class.
        /// </summary>
        /// <param name="logic">the first parameter of the constuctor.</param>
        public MainViewModel(IGameLogic logic)
        {
            this.logic = logic;
            this.Library = new ObservableCollection<Game>();

            if (this.IsInDesignMode)
            {
                Game p2 = new Game() { Name = "Wild Bill 2" };
                Game p3 = new Game() { Name = "Wild Bill 3" };
                this.Library.Add(p2);
                this.Library.Add(p3);
            }

            this.AddCmd = new RelayCommand(() => this.logic.AddGame(this.Library));
            this.ModCmd = new RelayCommand(() => this.logic.ModGame(this.SelectedGame));
            this.DelCmd = new RelayCommand(() => this.logic.DelGame(this.Library, this.SelectedGame));
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MainViewModel"/> class.
        /// </summary>
        public MainViewModel()
            : this(IsInDesignModeStatic ? null : ServiceLocator.Current.GetInstance<IGameLogic>())
        {
        }

        /// <summary>
        /// Gets the name of the customer.
        /// </summary>
        public ObservableCollection<Game> Library { get; private set; }

        /// <summary>
        /// Gets or sets documentation for the Library.
        /// </summary>
        public Game SelectedGame
        {
            get { return this.selectedGame; }
            set { this.Set(ref this.selectedGame, value); }
        }

        /// <summary>
        /// Gets documentation for the AddCmd.
        /// </summary>
        public ICommand AddCmd { get; private set; }

        /// <summary>
        /// Gets documentation for the ModCmd.
        /// </summary>
        public ICommand ModCmd { get; private set; }

        /// <summary>
        /// Gets documentation for the DelCmd.
        /// </summary>
        public ICommand DelCmd { get; private set; }
    }
}