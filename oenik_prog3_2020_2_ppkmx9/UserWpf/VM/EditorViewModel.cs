﻿// <copyright file="EditorViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace UserWpf.VM
{
    using GalaSoft.MvvmLight;
    using UserWpf.Data;

    /// <summary>
    /// Documentation of the internal EditorViewModel.
    /// </summary>
    internal class EditorViewModel : ViewModelBase
    {
        private Game game;

        /// <summary>
        /// Initializes a new instance of the <see cref="EditorViewModel"/> class.
        /// </summary>
        public EditorViewModel()
        {
            this.game = new Game();
            if (this.IsInDesignMode)
            {
                this.game.GameTime = 20;
                this.game.Name = "csé";
                this.game.Rating = 1;
                this.game.Genre = "shooter";
            }
        }

        /// <summary>
        /// Gets or sets documentation for the Game.
        /// </summary>
        public Game Game
        {
            get { return this.game; }
            set { this.Set(ref this.game, value); }
        }
    }
}