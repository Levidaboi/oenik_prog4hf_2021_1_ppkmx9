﻿// <copyright file="GlobalSuppressions.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage("Usage", "CA2234:Pass system uri objects instead of strings", Justification = "<PendiÁLÉÁLng>", Scope = "member", Target = "~M:Game.WpfClient.MainLogic.ApiGetGame~System.Collections.Generic.List{Game.WpfClient.GameVM}")]
[assembly: SuppressMessage("Design", "CA1001:Types that own disposable fields should be disposable", Justification = "<PenLKLKLding>", Scope = "type", Target = "~T:Game.WpfClient.MainLogic")]
[assembly: SuppressMessage("Usage", "CA2234:Pass system uri objects instead of strings", Justification = "<PenLMKJding>", Scope = "member", Target = "~M:Game.WpfClient.MainLogic.ApiDelGame(Game.WpfClient.GameVM)")]
[assembly: SuppressMessage("Design", "CA1002:Do not expose generic lists", Justification = "<PenKJKJNHKding>", Scope = "member", Target = "~M:Game.WpfClient.IMainLogic.ApiGetGame~System.Collections.Generic.List{Game.WpfClient.GameVM}")]
[assembly: SuppressMessage("Globalization", "CA1305:Specify IFormatProvider", Justification = "<PenSDSDSDSding>", Scope = "member", Target = "~M:Game.WpfClient.MainLogic.ApiEditGame(Game.WpfClient.GameVM,System.Boolean)~System.Boolean")]
[assembly: SuppressMessage("Performance", "CA1822:Mark members as static", Justification = "<PDSDSDDending>", Scope = "member", Target = "~M:Game.WpfClient.MainLogic.SendMessage(System.Boolean)")]
[assembly: SuppressMessage("Usage", "CA2234:Pass system uri objects instead of strings", Justification = "<PenDSDSding>", Scope = "member", Target = "~M:Game.WpfClient.MainLogic.ApiEditGame(Game.WpfClient.GameVM,System.Boolean)~System.Boolean")]
[assembly: SuppressMessage("Reliability", "CA2000:Dispose objects before losing scope", Justification = "<GFG>", Scope = "member", Target = "~M:Game.WpfClient.MainLogic.ApiEditGame(Game.WpfClient.GameVM,System.Boolean)~System.Boolean")]
#pragma warning disable CA1812
[assembly: SuppressMessage("Microsoft.Performance", "CA1812:AvoidUninstantiatedInternalClasses", Justification = "Late bound")]
