﻿// <copyright file="MainVm.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Game.WpfClient
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Input;
    using CommonServiceLocator;
    using GalaSoft.MvvmLight;
    using GalaSoft.MvvmLight.Command;

    /// <summary>
    /// Documentation of the internal MainVM class.
    /// </summary>
    internal class MainVM : ViewModelBase
    {
        private GameVM gameView;
        private ObservableCollection<GameVM> games;
        private IMainLogic logic;

        /// <summary>
        /// Initializes a new instance of the <see cref="MainVM"/> class.
        /// </summary>
        /// <param name="logic">Logic portion of business logic for frontend methods.</param>
        public MainVM(IMainLogic logic)
        {
            this.logic = logic;
            this.LoadCmd = new RelayCommand(() => this.Games = new ObservableCollection<GameVM>(
            this.logic.ApiGetGame()));
            this.DelCmd = new RelayCommand(() => this.logic.ApiDelGame(this.gameView));
            this.AddCmd = new RelayCommand(() => this.logic.EditGame(null, this.EditorFunc));
            this.ModCmd = new RelayCommand(() => this.logic.EditGame(this.gameView, this.EditorFunc));
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MainVM"/> class.
        /// </summary>
        public MainVM()
            : this(IsInDesignModeStatic ? null : ServiceLocator.Current.GetInstance<IMainLogic>()) // IoC
        {
        }

        /// <summary>
        /// Gets or sets the property of editor function.
        /// </summary>
        public Func<GameVM, bool> EditorFunc { get; set; }

        /// <summary>
        /// Gets or sets the property of game collection.
        /// </summary>
        public ObservableCollection<GameVM> Games
        {
            get { return this.games; }
            set { this.Set(ref this.games, value); }
        }

        /// <summary>
        /// Gets or sets the game which is currently selected from the frontend list.
        /// </summary>
        public GameVM GameVM
        {
            get { return this.gameView; }
            set { this.Set(ref this.gameView, value); }
        }

        /// <summary>
        /// Gets Add Command.
        /// </summary>
        public ICommand AddCmd { get; private set; }

        /// <summary>
        /// Gets the Load Command.
        /// </summary>
        public ICommand LoadCmd { get; private set; }

        /// <summary>
        /// Gets Modify Command.
        /// </summary>
        public ICommand ModCmd { get; private set; }

        /// <summary>
        /// Gets Delete Command.
        /// </summary>
        public ICommand DelCmd { get; private set; }
    }
}
