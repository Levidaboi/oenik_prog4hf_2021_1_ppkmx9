﻿// <copyright file="MainLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Game.WpfClient
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net.Http;
    using System.Text.Json;
    using GalaSoft.MvvmLight.Messaging;

    /// <summary>
    /// Main logic of the basic crud methods for games api.
    /// </summary>
    internal class MainLogic : IMainLogic
    {
        private string url = "http://localhost:5000/GameApi/";
        private HttpClient client = new HttpClient();
        private JsonSerializerOptions jsonOptions = new JsonSerializerOptions(
            JsonSerializerDefaults.Web);

        /// <summary>
        /// Gets all the games from an api endpoint.
        /// </summary>
        /// <returns>All the games.</returns>
        public List<GameVM> ApiGetGame()
        {
            string json = this.client.GetStringAsync(this.url + "all").Result;
            var list = JsonSerializer.Deserialize<List<GameVM>>(json, this.jsonOptions);

            return list;
        }

        /// <summary>
        /// Deletes a game store if possible.
        /// </summary>
        /// <param name="game">A game which is intended to delete.</param>
        public void ApiDelGame(GameVM game)
        {
            bool sucess = false;
            if (game != null)
            {
                string json = this.client.GetStringAsync(this.url + "del/" + game.GameId).Result;
                JsonDocument doc = JsonDocument.Parse(json);
                sucess = doc.RootElement.EnumerateObject().First().Value.GetRawText() == "true";
            }

            this.SendMessage(sucess);
        }

        /// <summary>
        /// Edit or add a new game .
        /// </summary>
        /// <param name="game">game which is intended to add or modify.</param>
        /// <param name="editorFunc">The function part of the editing process.</param>
        public void EditGame(GameVM game, Func<GameVM, bool> editorFunc)
        {
            GameVM clone = new GameVM();
            if (game != null)
            {
                clone.CopyFrom(game);
            }

            bool? success = editorFunc?.Invoke(clone);

            if (success == true)
            {
                if (game != null)
                {
                    success = this.ApiEditGame(clone, true);
                }
                else
                {
                    success = this.ApiEditGame(clone, false);
                }
            }

            this.SendMessage(success == true);
        }

        /// <summary>
        /// Returns a message whether an Api call was successful.
        /// </summary>
        /// <param name="success">Defines whether an api call was sucessful.</param>
        private void SendMessage(bool success)
        {
            string msg = success ? "Operation completed successfully" : "Operation failed";
            Messenger.Default.Send(msg, "GameResult");
        }

        private bool ApiEditGame(GameVM game, bool isEditing)
        {
            if (game == null)
            {
                return false;
            }

            string myUrl = isEditing ? this.url + "mod" : this.url + "add";
            Dictionary<string, string> postData = new Dictionary<string, string>();
            if (isEditing)
            {
                postData.Add("game_id", game.GameId);
            }

            postData.Add("name", game.Name);
            postData.Add("genre", game.Genre);
            postData.Add("gametime", game.GameTime.ToString());
            string json = this.client.PostAsync(myUrl, new FormUrlEncodedContent(postData)).
                Result.Content.ReadAsStringAsync().Result;
            JsonDocument doc = JsonDocument.Parse(json);
            return doc.RootElement.EnumerateObject().First().Value.GetRawText() == "true";
        }
    }
}
