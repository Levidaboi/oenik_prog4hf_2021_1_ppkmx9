﻿// <copyright file="IMainLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Game.WpfClient
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Documentation of the public GameVM interface.
    /// </summary>
    public interface IMainLogic
    {
        /// <summary>
        /// gets the games.
        /// </summary>
        /// <returns>the games from the database.</returns>
        List<GameVM> ApiGetGame();

        /// <summary>
        /// edits a game.
        /// </summary>
        /// <param name="game">The first name to join.</param>
        /// <param name="editorFunc">The last name to join.</param>
        void EditGame(GameVM game, Func<GameVM, bool> editorFunc);

        /// <summary>
        /// deletes a game.
        /// </summary>
        /// <param name="game">The first name to join.</param>
        void ApiDelGame(GameVM game);
    }
}
