﻿// <copyright file="GameVM.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

[assembly: System.CLSCompliant(false)]

namespace Game.WpfClient
{
    using GalaSoft.MvvmLight;

    /// <summary>
    /// Documentation of the public GameVM class.
    /// </summary>
    public class GameVM : ObservableObject
    {
        private string gameid;
        private string name;
        private string genre;
        private int gametime;

        /// <summary>
        /// Gets or sets documentation for the Name.
        /// </summary>
        public string GameId
        {
            get { return this.gameid; }
            set { this.Set(ref this.gameid, value); }
        }

        /// <summary>
        /// Gets or sets documentation for the Name.
        /// </summary>
        public string Name
        {
            get { return this.name; }
            set { this.Set(ref this.name, value); }
        }

        /// <summary>
        /// Gets or sets documentation for the Genre.
        /// </summary>
        public string Genre
        {
            get { return this.genre; }
            set { this.Set(ref this.genre, value); }
        }

        /// <summary>
        /// Gets or sets documentation for the GameTime.
        /// </summary>
        public int GameTime
        {
            get { return this.gametime; }
            set { this.Set(ref this.gametime, value); }
        }

        /// <summary>
        /// copies the props from the parameter.
        /// </summary>
        /// <param name="other">the first parameter of the method.</param>
        public void CopyFrom(GameVM other)
        {
            if (other == null)
            {
                return;
            }

            this.GameId = other.GameId;
            this.GameTime = other.GameTime;
            this.Name = other.Name;
            this.Genre = other.Genre;
        }
    }
}
