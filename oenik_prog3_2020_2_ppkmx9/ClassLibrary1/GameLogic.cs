﻿// <copyright file="GameLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Logic
{
    using System.Collections.Generic;
    using System.Linq;
    using Models;
    using Repository;

    /// <summary>
    /// Documentation of the public GameLogic class.
    /// </summary>
    public class GameLogic
    {
        private IRepo<Game> gameRepo;

        /// <summary>
        /// Initializes a new instance of the <see cref="GameLogic"/> class.
        /// </summary>
        /// <param name="gameRepo">the second parameter of the constuctor.</param>
        public GameLogic(IRepo<Game> gameRepo)
        {
            this.gameRepo = gameRepo;
        }

        /// <summary>
        /// Adds the game to the database and saves it.
        /// </summary>
        /// <param name="game">the first parameter of the method .</param>
        public void AddGame(Game game)
        {
            this.gameRepo.Add(game);
            this.gameRepo.Save();
        }

        /// <summary>
        /// Deletes the game to the database and saves it.
        /// </summary>
        /// <param name="id">the first parameter of the method.</param>
        public void DeleteGame(string id)
        {
            Game g = this.GetGame(id);
            this.gameRepo.Delete(g);
        }

        /// <summary>
        /// gets all the games from the database.
        /// </summary>
        /// <returns> games as IQueryable .</returns>
        public IQueryable<Game> GetAllGames()
        {
            return this.gameRepo.AllItem();
        }

        /// <summary>
        /// gets a games from the database by it's id.
        /// </summary>
        /// <param name="gameid">the first parameter of the method.</param>
        /// <returns> Game .</returns>
        public Game GetGame(string gameid)
        {
            return this.gameRepo.Read(gameid);
        }

        /// <summary>
        /// Updates a game in the database.
        /// </summary>
        /// <param name="oldid">the first parameter of the method.</param>
        /// <param name="newitem">the second parameter of the method .</param>
        public void UpdateGame(string oldid, Game newitem)
        {
            this.gameRepo.Update(oldid, newitem);
        }

        /// <summary>
        /// Sums the gametime from all the games in the database.
        /// </summary>
        /// <returns> the summed gametime .</returns>
        public int SumGameTime()
        {
            List<Game> games = this.gameRepo.AllItem().ToList();
            int gameTimeSum = (from x in games
                               select x.GameTime).Sum();

            return gameTimeSum;
        }

        /// <summary>
        /// Sums the achipoints from a game.
        /// </summary>
        /// <param name="gameId">the first parameter of the method which.</param>
        /// <returns> the summed achiPoints .</returns>
        public int GetAchiPoints(string gameId)
        {
            int achiPoints = 0;
            Game g = this.GetGame(gameId);

            achiPoints = (from x in g.Achievements
                          select (int)x.AchiLevel).Sum();

            return achiPoints;
        }
    }
}