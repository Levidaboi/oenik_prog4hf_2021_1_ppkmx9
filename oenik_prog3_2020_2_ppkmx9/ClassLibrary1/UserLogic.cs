﻿// <copyright file="UserLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Logic
{
    using System.Linq;
    using Models;
    using Repository;

    /// <summary>
    /// Documentation of the public UserLogic class.
    /// </summary>
    public class UserLogic
    {
        private IRepo<User> userrepo;

        private IRepo<Game> gameRepo;

        private GameLogic gameLogic;

        private IRepo<Achievement> achievementRepo;

        /// <summary>
        /// Initializes a new instance of the <see cref="UserLogic"/> class.
        /// </summary>
        /// <param name="userrepo">the first parameter of the constuctor.</param>
        /// <param name="gameRepo">the second parameter of the constuctor.</param>
        /// <param name="achievementRepo">the third parameter of the constuctor.</param>
        public UserLogic(IRepo<User> userrepo, IRepo<Game> gameRepo, IRepo<Achievement> achievementRepo)
        {
            this.achievementRepo = achievementRepo;
            this.gameRepo = gameRepo;
            this.userrepo = userrepo;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="UserLogic"/> class.
        /// </summary>
        /// <param name="userrepo">the first parameter of the constuctor.</param>
        public UserLogic(IRepo<User> userrepo)
        {
            this.userrepo = userrepo;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="UserLogic"/> class.
        /// </summary>
        /// <param name="userrepo">the first parameter of the constuctor.</param>
        /// <param name="gameLogic">the second parameter of the constuctor.</param>
        /// <param name="gameRepo">the third parameter of the constuctor.</param>
        /// <param name="achievementRepo">the fourth parameter of the constuctor.</param>
        public UserLogic(IRepo<User> userrepo, GameLogic gameLogic, IRepo<Game> gameRepo, IRepo<Achievement> achievementRepo)
        {
            this.achievementRepo = achievementRepo;
            this.gameRepo = gameRepo;
            this.gameLogic = gameLogic;
            this.userrepo = userrepo;
        }

        /// <summary>
        /// Calls the repository's Add method.
        /// </summary>
        /// <param name="u">the first parameter of the method whiich is a User.</param>
        public void AddNewUser(User u)
        {
            this.userrepo.Add(u);
        }

        /// <summary>
        /// Calls the Delete method from userrepo which deletes a user.
        /// </summary>
        /// <param name="uid">the first parameter of the method.</param>
        public void DeleteUser(string uid)
        {
            this.userrepo.Delete(uid);
        }

        /// <summary>
        /// Calls the AllItem method from UserRepository.
        /// </summary>
        /// <returns>All Users from the database as IQueryable.</returns>
        public IQueryable<User> GetUsers()
        {
            return this.userrepo.AllItem();
        }

        /// <summary>
        /// gets user by it's id from the database.
        /// </summary>
        /// <param name="uid">the user's id.</param>
        /// <returns> User .</returns>
        public User GetUser(string uid)
        {
            return this.userrepo.Read(uid);
        }

        /// <summary>
        /// updates user by it's id from the database.
        /// </summary>
        /// <param name="oldid">the user's id.</param>
        /// <param name="newitem">user with the new properties.</param>
        public void UpdateUser(string oldid, User newitem)
        {
            this.userrepo.Update(oldid, newitem);
        }

        /// <summary>
        /// counts the user's games.
        /// </summary>
        /// <param name="userid">the user's id.</param>
        /// <returns> the game's count.</returns>
        public int GetGameCount(string userid)
        {
            int i = (from x in this.GetUser(userid).GameLibrary
                     select x).Count();

            return i;
        }

        /// <summary>
        /// sums the user's game's gametime.
        /// </summary>
        /// <param name="userid">the user's id.</param>
        /// <returns> the summed up gametime.</returns>
        public int GetGameTime(string userid)
        {
            int i = (from x in this.GetUser(userid).GameLibrary
                     select x.GameTime).Sum();

            return i;
        }

        /// <summary>
        /// sums the user's game's achievement points.
        /// </summary>
        /// <param name="userid">the user's id.</param>
        /// <returns> the summed up achievement points.</returns>
        public int GetAchiPoints(string userid)
        {
            User u = this.GetUser(userid);
            int i = 0;

            foreach (var game in u.GameLibrary)
            {
                Game g = this.gameLogic.GetGame(game.GameId);
                i += this.gameLogic.GetAchiPoints(game.GameId);
            }

            return i;
        }

        /// <summary>
        /// finds the user with the most achievement points.
        /// </summary>
        /// <returns> the user's name.</returns>
        public string GetBestGamer()
        {
            var q = (from x in this.userrepo.AllItem().ToList()
                     join y in this.gameRepo.AllItem().ToList() on x.UserId equals y.UserId
                     join z in this.achievementRepo.AllItem().ToList() on y.GameId equals z.GameId
                     group x by x.Name into g
                     select new
                     {
                         Name = g.Key,
                         points = g.SelectMany(x => x.GameLibrary).SelectMany(y => y.Achievements).Distinct().Sum(z => (int)z.AchiLevel),
                     }).OrderByDescending(x => x.points).FirstOrDefault();

            return q.Name;
        }

        /// <summary>
        /// finds the user with the most gametime.
        /// </summary>
        /// <returns> the user's name.</returns>
        public string GetLifelessGamer()
        {
            var q = (from x in this.userrepo.AllItem().ToList()
                     join y in this.gameRepo.AllItem().ToList() on x.UserId equals y.UserId
                     group x by x.Name into g
                     select new
                     {
                         name = g.Key,
                         time = g.SelectMany(x => x.GameLibrary).Distinct().Sum(y => y.GameTime),
                     }).OrderByDescending(x => x.time).FirstOrDefault();

            return q.name;
        }

        /// <summary>
        /// finds the user with the best game ratings .
        /// </summary>
        /// <returns> the user's name.</returns>
        public string GetPickiestGamer()
        {
            var q = (from x in this.userrepo.AllItem().ToList()
                     join y in this.gameRepo.AllItem().ToList() on x.UserId equals y.UserId
                     group x by x.Name into g
                     select new
                     {
                         name = g.Key,
                         avg = g.SelectMany(x => x.GameLibrary).Distinct().Average(y => y.Rating),
                     }).OrderByDescending(x => x.avg).FirstOrDefault();

            return q.name;
        }

        /// <summary>
        /// finds the user with the most games.
        /// </summary>
        /// <returns> the user's name.</returns>
        public string GetRichestGamer()
        {
            var q = (from x in this.userrepo.AllItem().ToList()
                     select new
                     {
                         name = x.Name,
                         GameCount = x.GameLibrary.Count,
                     }).OrderByDescending(x => x.GameCount).FirstOrDefault();

            return q.name;
        }
    }
}