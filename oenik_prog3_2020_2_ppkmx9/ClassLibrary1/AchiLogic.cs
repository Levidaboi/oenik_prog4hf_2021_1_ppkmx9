﻿// <copyright file="AchiLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

[assembly: System.CLSCompliant(false)]

namespace Logic
{
    using System.Linq;
    using Models;
    using Repository;

    /// <summary>
    /// Documentation of the public AchiLogic class.
    /// </summary>
    public class AchiLogic
    {
        private IRepo<Achievement> achiRepo;

        /// <summary>
        /// Initializes a new instance of the <see cref="AchiLogic"/> class.
        /// </summary>
        /// <param name="achiRepo">the second parameter of the constuctor.</param>
        public AchiLogic(IRepo<Achievement> achiRepo)
        {
            this.achiRepo = achiRepo;
        }

        /// <summary>
        /// Adds the achi to the database and saves it.
        /// </summary>
        /// <param name="a">the first parameter of the method.</param>
        public void AddAchi(Achievement a)
        {
            this.achiRepo.Add(a);
        }

        /// <summary>
        /// Deletes the achi to the database and saves it.
        /// </summary>
        /// <param name="a">the first parameter of the method.</param>
        public void DeleteAchi(Achievement a)
        {
            this.achiRepo.Delete(a);
        }

        /// <summary>
        /// gets all the achies from the database.
        /// </summary>
        /// <returns> achis as IQueryable .</returns>
        public IQueryable<Achievement> GetAchievements()
        {
            return this.achiRepo.AllItem();
        }

        /// <summary>
        /// gets an achi from the database by it's id.
        /// </summary>
        /// <param name="achiId">the first parameter of the method.</param>
        /// <returns> Achi .</returns>
        public Achievement GetAchievement(string achiId)
        {
            return this.achiRepo.Read(achiId);
        }

        /// <summary>
        /// Updates an achi in the database.
        /// </summary>
        /// <param name="oldid">the first parameter of the method.</param>
        /// <param name="newitem">the second parameter of the method .</param>
        public void UpdateAchi(string oldid, Achievement newitem)
        {
            this.achiRepo.Update(oldid, newitem);
        }
    }
}