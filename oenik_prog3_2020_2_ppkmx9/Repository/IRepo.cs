﻿// <copyright file="IRepo.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Repository
{
    using System;
    using System.Linq;

    /// <summary>
    /// Documentation of the public IRepo interface.
    /// </summary>
    /// <typeparam name = "T" >The first generic type parameter.</typeparam>
    public interface IRepo<T>
        where T : new()
    {
        /// <summary>
        /// adds a item to the database.
        /// </summary>
        /// <param name="item">the item.</param>
        void Add(T item);

        /// <summary>
        /// deletes a item from the database.
        /// </summary>
        /// <param name="item">the item.</param>
        void Delete(T item);

        /// <summary>
        /// deletes a item by it's id from the database.
        /// </summary>
        /// <param name="uid">the item's id.</param>
        void Delete(string uid);

        /// <summary>
        /// gets item by it's id from the database.
        /// </summary>
        /// <param name="uid">the item's id.</param>
        /// <returns> item .</returns>
        T Read(string uid);

        /// <summary>
        /// gets all item from the database.
        /// </summary>
        /// <returns> item as IQueryable.</returns>
        IQueryable<T> AllItem();

        /// <summary>
        /// updates item by it's id from the database.
        /// </summary>
        /// <param name="oldid">the item's id.</param>
        /// <param name="newitem">item with the new properties.</param>
        void Update(string oldid, T newitem);

        /// <summary>
        /// Saves the database.
        /// </summary>
        void Save();
    }
}
