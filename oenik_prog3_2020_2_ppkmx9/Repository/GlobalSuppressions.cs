﻿// <copyright file="GlobalSuppressions.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage("Design", "CA1062:Validate arguments of public methods", Justification = "useless", Scope = "member", Target = "~M:Repository.AchievementRepo.Update(System.String,Models.Achievement)")]
[assembly: SuppressMessage("Design", "CA1001:Types that own disposable fields should be disposable", Justification = "useless", Scope = "type", Target = "~T:Repository.AchievementRepo")]
[assembly: SuppressMessage("Design", "CA1001:Types that own disposable fields should be disposable", Justification = "useless", Scope = "type", Target = "~T:Repository.UserRepo")]
[assembly: SuppressMessage("Design", "CA1062:Validate arguments of public methods", Justification = "useless", Scope = "member", Target = "~M:Repository.UserRepo.Update(System.String,Models.User)")]
[assembly: SuppressMessage("Design", "CA1062:Validate arguments of public methods", Justification = "useless", Scope = "member", Target = "~M:Repository.GameRepo.Update(System.String,Models.Game)")]
[assembly: SuppressMessage("Design", "CA1001:Types that own disposable fields should be disposable", Justification = "useless", Scope = "type", Target = "~T:Repository.GameRepo")]
