﻿// <copyright file="GameRepo.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Data;
    using Models;

    /// <summary>
    /// Documentation of the public GameRepo class.
    /// </summary>
    public class GameRepo : IRepo<Game>
    {
        private UserDbContext db = new UserDbContext();

        /// <inheritdoc/>
        public void Add(Game item)
        {
            this.db.Games.Add(item);
            this.Save();
        }

        /// <inheritdoc/>
        public IQueryable<Game> AllItem()
        {
            return this.db.Games.AsQueryable();
        }

        /// <inheritdoc/>
        public void Delete(Game item)
        {
            this.db.Games.Remove(item);
            this.Save();
        }

        /// <inheritdoc/>
        public void Delete(string uid)
        {
            Game item = this.Read(uid);
            this.db.Games.Remove(item);
            this.Save();
        }

        /// <inheritdoc/>
        public Game Read(string uid)
        {
            Game item = (from x in this.db.Games
                        where x.GameId == uid
                        select x).FirstOrDefault();
            return item;
        }

        /// <inheritdoc/>
        public void Save()
        {
            this.db.SaveChanges();
        }

        /// <inheritdoc/>
        public void Update(string oldid, Game newitem)
        {
            Game olditem = this.Read(oldid);
            olditem.Name = newitem.Name;
            olditem.Genre = newitem.Genre;
            olditem.GameTime = newitem.GameTime;
            olditem.Rating = newitem.Rating;
            this.Save();
        }
    }
}
