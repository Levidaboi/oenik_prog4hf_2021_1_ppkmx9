﻿// <copyright file="AchievementRepo.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

[assembly: System.CLSCompliant(false)]

namespace Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Data;
    using Models;

    /// <summary>
    /// Documentation of the public AchievementRepo class.
    /// </summary>
    public class AchievementRepo : IRepo<Achievement>
    {
        private UserDbContext db = new UserDbContext();

        /// <inheritdoc/>
        public void Add(Achievement item)
        {
            this.db.Achievements.Add(item);
            this.Save();
        }

        /// <inheritdoc/>
        public IQueryable<Achievement> AllItem()
        {
            return this.db.Achievements.AsQueryable();
        }

        /// <inheritdoc/>
        public void Delete(Achievement item)
        {
            this.db.Achievements.Remove(item);
            this.Save();
        }

        /// <inheritdoc/>
        public void Delete(string uid)
        {
            Achievement item = this.Read(uid);
            this.db.Achievements.Remove(item);
            this.Save();
        }

        /// <inheritdoc/>
        public Achievement Read(string uid)
        {
            Achievement item = (from x in this.db.Achievements
                                where x.AchiId == uid
                                select x).FirstOrDefault();
            return item;
        }

        /// <inheritdoc/>
        public void Save()
        {
            this.db.SaveChanges();
        }

        /// <inheritdoc/>
        public void Update(string oldid, Achievement newitem)
        {
            Achievement olditem = this.Read(oldid);
            olditem.Name = newitem.Name;
            olditem.AchiLevel = newitem.AchiLevel;

            this.Save();
        }
    }
}