﻿// <copyright file="UserRepo.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Repository
{
    using System;
    using System.Linq;
    using Data;
    using Models;

    /// <summary>
    /// Documentation of the public UserRepo class.
    /// </summary>
    public class UserRepo : IRepo<User>
    {
        private UserDbContext db = new UserDbContext();

        /// <summary>
        /// adds a user to the database.
        /// </summary>
        /// <param name="item">the user.</param>
        public void Add(User item)
        {
            this.db.Users.Add(item);
            this.Save();
        }

        /// <summary>
        /// deletes a user from the database.
        /// </summary>
        /// <param name="item">the user.</param>
        public void Delete(User item)
        {
            this.db.Users.Remove(item);
            this.Save();
        }

        /// <summary>
        /// deletes a user by it's id from the database.
        /// </summary>
        /// <param name="item">the user's id.</param>
        public void Delete(string item)
        {
            User user = this.Read(item);
            this.db.Users.Remove(user);
            this.Save();
        }

        /// <summary>
        /// gets user by it's id from the database.
        /// </summary>
        /// <param name="uid">the user's id.</param>
        /// <returns> User .</returns>
        public User Read(string uid)
        {
            User item = (from x in this.db.Users
                         where x.UserId == uid
                         select x).FirstOrDefault();
            return item;
        }

        /// <summary>
        /// gets all users from the database.
        /// </summary>
        /// <returns> Users as IQueryable.</returns>
        public IQueryable<User> AllItem()
        {
            return this.db.Users.AsQueryable();
        }

        /// <summary>
        /// Saves the database.
        /// </summary>
        public void Save()
        {
            this.db.SaveChanges();
        }

        /// <summary>
        /// updates user by it's id from the database.
        /// </summary>
        /// <param name="oldid">the user's id.</param>
        /// <param name="newitem">user with the new properties.</param>
        public void Update(string oldid, User newitem)
        {
            User olduser = this.Read(oldid);

            olduser.Name = newitem.Name;
            olduser.Age = newitem.Age;

            this.Save();
        }
    }
}