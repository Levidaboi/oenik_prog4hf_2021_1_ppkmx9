﻿// <copyright file="GlobalSuppressions.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage("Naming", "CA1707:Identifiers should not contain underscores", Justification = "useless", Scope = "namespace", Target = "~N:Feleves_feladat.Controllers")]
[assembly: SuppressMessage("Design", "CA1062:Validate arguments of public methods", Justification = "useless", Scope = "member", Target = "~M:Feleves_feladat.Controllers.HomeController.AddAchievement(Models.Achievement)~Microsoft.AspNetCore.Mvc.IActionResult")]
[assembly: SuppressMessage("Design", "CA1062:Validate arguments of public methods", Justification = "useless", Scope = "member", Target = "~M:Feleves_feladat.Controllers.HomeController.AddGame(Models.Game)~Microsoft.AspNetCore.Mvc.IActionResult")]
[assembly: SuppressMessage("Design", "CA1062:Validate arguments of public methods", Justification = "useless", Scope = "member", Target = "~M:Feleves_feladat.Controllers.HomeController.AddUser(Models.User)~Microsoft.AspNetCore.Mvc.IActionResult")]
[assembly: SuppressMessage("Design", "CA1062:Validate arguments of public methods", Justification = "useless", Scope = "member", Target = "~M:Feleves_feladat.Controllers.HomeController.EditAchi(Models.Achievement)~Microsoft.AspNetCore.Mvc.IActionResult")]
[assembly: SuppressMessage("Design", "CA1062:Validate arguments of public methods", Justification = "useless", Scope = "member", Target = "~M:Feleves_feladat.Controllers.HomeController.EditGame(Models.Game)~Microsoft.AspNetCore.Mvc.IActionResult")]
[assembly: SuppressMessage("Design", "CA1062:Validate arguments of public methods", Justification = "useless", Scope = "member", Target = "~M:Feleves_feladat.Controllers.HomeController.EditUser(Models.User)~Microsoft.AspNetCore.Mvc.IActionResult")]
[assembly: SuppressMessage("StyleCop.CSharp.MaintainabilityRules", "SA1404:Code analysis suppression should have justification", Justification = "useless")]
[assembly: SuppressMessage("Design", "CA1052:Static holder types should be Static or NotInheritable", Justification = "useless", Scope = "type", Target = "~T:Feleves_feladat.Program")]

[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage(
    "Microsoft.Naming",
    "CA1707:IdentifiersShouldNotContainUnderscores",
    Justification = "Test methods require underscores for readability.",
    Scope = "module")
]
[assembly: SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification = "<Pendasdasdasing>", Scope = "member", Target = "~M:Feleves_feladat.Controllers.GameApiController.UpdateGame(Feleves_feladat.Model.GameModel)~Feleves_feladat.Model.ApiResult")]
[assembly: SuppressMessage("Design", "CA1062:Validate arguments of public methods", Justification = "<Pfadsfafending>", Scope = "member", Target = "~M:Feleves_feladat.Controllers.GameApiController.UpdateGame(Feleves_feladat.Model.GameModel)~Feleves_feladat.Model.ApiResult")]
[assembly: SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification = "<Pendisadsdsadng>", Scope = "member", Target = "~M:Feleves_feladat.Controllers.GameApiController.AddGame(Feleves_feladat.Model.GameModel)~Feleves_feladat.Model.ApiResult")]
[assembly: SuppressMessage("Design", "CA1051:Do not declare visible instance fields", Justification = "<Pengfgfding>", Scope = "member", Target = "~F:Feleves_feladat.Controllers.GameApiController.Logic")]
[assembly: SuppressMessage("StyleCop.CSharp.MaintainabilityRules", "SA1401:Fields should be private", Justification = "<Pendinhghg>", Scope = "member", Target = "~F:Feleves_feladat.Controllers.GameApiController.Logic")]
[assembly: SuppressMessage("StyleCop.CSharp.MaintainabilityRules", "SA1401:Fields should be private", Justification = "<Pevbvbvnding>", Scope = "member", Target = "~F:Feleves_feladat.Controllers.GameApiController.mapper")]
[assembly: SuppressMessage("Design", "CA1051:Do not declare visible instance fields", Justification = "<Penddsdsding>", Scope = "member", Target = "~F:Feleves_feladat.Controllers.GameApiController.mapper")]
[assembly: SuppressMessage("StyleCop.CSharp.NamingRules", "SA1307:Accessible fields should begin with upper-case letter", Justification = "<Pedasdsdnding>", Scope = "member", Target = "~F:Feleves_feladat.Controllers.GameApiController.mapper")]
[assembly: SuppressMessage("Design", "CA1062:Validate arguments of public methods", Justification = "<Pendidsdsdng>", Scope = "member", Target = "~M:Feleves_feladat.Controllers.GameApiController.AddGame(Feleves_feladat.Model.GameModel)~Feleves_feladat.Model.ApiResult")]
