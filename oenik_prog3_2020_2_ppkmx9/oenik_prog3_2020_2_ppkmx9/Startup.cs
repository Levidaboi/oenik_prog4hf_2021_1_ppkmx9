// <copyright file="Startup.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Feleves_feladat
{
    using AutoMapper;
    using Feleves_feladat.Model;
    using Logic;
    using Microsoft.AspNetCore.Builder;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.Extensions.Hosting;
    using Models;
    using Repository;

    /// <summary>
    /// Documentation of the public Startup class.
    /// </summary>
    public class Startup
    {
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940

        /// <summary>
        /// Sets up the dependency injection in the project.
        /// </summary>
        /// <param name="services">the first parameter of the method.</param>
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddTransient<IRepo<User>, UserRepo>();
            services.AddTransient<UserLogic, UserLogic>();
            services.AddTransient<GameLogic, GameLogic>();
            services.AddTransient<IRepo<Game>, GameRepo>();
            services.AddTransient<AchiLogic, AchiLogic>();
            services.AddTransient<IRepo<Achievement>, AchievementRepo>();
            services.AddSingleton<IMapper>(provider => MapperFactory.CreateMapper());
            services.AddMvc(opt => opt.EnableEndpointRouting = false);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.

        /// <summary>
        /// Sets up the dependency injection in the project.
        /// </summary>
        /// <param name="app">the first parameter of the method.</param>
        /// <param name="env">the second parameter of the method.</param>
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseMvcWithDefaultRoute();
            app.UseRouting();

            app.UseStaticFiles();
        }
    }
}