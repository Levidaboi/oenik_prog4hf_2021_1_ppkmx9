// <copyright file="MapperFactory.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Feleves_feladat.Model
{
    using AutoMapper;

    /// <summary>
    /// Factory for the auto mapper.
    /// </summary>
    public static class MapperFactory
    {
        /// <summary>
        /// Creates an auto mapper between kes_bolt and knifestore web models.
        /// </summary>
        /// <returns>The initialized configuration.</returns>
        public static IMapper CreateMapper()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Models.Game, GameModel>().
                    ForMember(dest => dest.GameId, map => map.MapFrom(src => src.GameId)).
                    ForMember(dest => dest.GameTime, map => map.MapFrom(src => src.GameTime)).
                    ForMember(dest => dest.Genre, map => map.MapFrom(src => src.Genre)).
                    ForMember(dest => dest.Name, map => map.MapFrom(src => src.Name));

                // ReverseMap();
            });
            return config.CreateMapper();
        }
    }
}