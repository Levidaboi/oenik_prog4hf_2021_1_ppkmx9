// <copyright file="GameModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Feleves_feladat.Model
{
    using System.ComponentModel.DataAnnotations;

    /// <summary>
    /// Web model for Game entites.
    /// </summary>
    public class GameModel
    {
        /// <summary>
        /// Gets or sets documentation for the GameId.
        /// </summary>
        public string GameId { get; set; }

        /// <summary>
        /// Gets or sets documentation for the Name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets documentation for the Genre.
        /// </summary>
        public string Genre { get; set; }

        /// <summary>
        /// Gets or sets documentation for the GameTime.
        /// </summary>
        public int GameTime { get; set; }
    }
}