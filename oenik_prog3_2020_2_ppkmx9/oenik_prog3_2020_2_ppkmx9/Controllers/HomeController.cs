﻿// <copyright file="HomeController.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Feleves_feladat.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Logic;
    using Microsoft.AspNetCore.Mvc;
    using Models;

    /// <summary>
    /// Documentation of the public HomeController class.
    /// </summary>
    public class HomeController : Controller
    {
        private UserLogic userLogic;
        private GameLogic gameLogic;
        private AchiLogic achiLogic;

        /// <summary>
        /// Initializes a new instance of the <see cref="HomeController"/> class.
        /// </summary>
        /// <param name="userLogic">the first parameter of the constuctor.</param>
        /// <param name="gameLogic">the second parameter of the constuctor.</param>
        /// <param name="achiLogic">the third parameter of the constuctor.</param>
        public HomeController(UserLogic userLogic, GameLogic gameLogic, AchiLogic achiLogic)
        {
            this.userLogic = userLogic;
            this.gameLogic = gameLogic;
            this.achiLogic = achiLogic;
        }

        /// <summary>
        /// Calls the gameLogic's SumGameTime method.
        /// </summary>
        /// <returns> A view with all the game time summed up .</returns>
        public IActionResult Index()
        {
            return this.View(this.gameLogic.SumGameTime());
        }

        /// <summary>
        /// Calls the userLogic's GetGameCount ,GetGameTime,GetAchiPoints method.
        /// </summary>
        /// <returns> A view with the statistics .</returns>
        public IActionResult Statistics()
        {
            List<Stat> stats = new List<Stat>();
            foreach (var user in this.userLogic.GetUsers().ToList())
            {
                Stat s = new Stat();
                s.PlayerName = user.Name;
                s.GameCount = this.userLogic.GetGameCount(user.UserId);
                s.GameTime = this.userLogic.GetGameTime(user.UserId);
                s.AchiPoints = this.userLogic.GetAchiPoints(user.UserId);

                stats.Add(s);
            }

            return this.View(stats);
        }

        /// <summary>
        /// Calls the userLogic's GetBestGamer ,GetLifelessGamer,GetRichestGamer  , GetPickiestGamer method.
        /// </summary>
        /// <returns> A view with the statistics .</returns>
        public IActionResult SpecificStats()
        {
            Stat2 stat = new Stat2();

            stat.BestGamer = this.userLogic.GetBestGamer();
            stat.LifelessGamer = this.userLogic.GetLifelessGamer();
            stat.RichestGamer = this.userLogic.GetRichestGamer();
            stat.PickiestGamer = this.userLogic.GetPickiestGamer();

            return this.View(stat);
        }

        /// <summary>
        /// Fills up the datatbase with sample data.
        /// </summary>
        /// <returns> A view to the Main page .</returns>
        public IActionResult GenerateData()
        {
            // u1
            User u = new User() { Name = "Blobbo", Age = 21 };
            u.UserId = Guid.NewGuid().ToString();
            this.userLogic.AddNewUser(u);

            Game g = new Game() { Name = "Serious Sam 4", Genre = "Shooter", UserId = u.UserId, Rating = 2, GameTime = 30 };
            g.GameId = Guid.NewGuid().ToString();
            this.gameLogic.AddGame(g);

            Game g9 = new Game() { Name = "League of legends", Genre = "MOBA", UserId = u.UserId, Rating = 1, GameTime = 1400 };
            g9.GameId = Guid.NewGuid().ToString();
            this.gameLogic.AddGame(g9);

            Achievement a = new Achievement() { Name = "Kitoltad", AchiLevel = AchiLevel.Gold, GameId = g.GameId };
            a.AchiId = Guid.NewGuid().ToString();
            this.achiLogic.AddAchi(a);

            Achievement a5 = new Achievement() { Name = "Saltiest boi in the west", AchiLevel = AchiLevel.Gold, GameId = g.GameId };
            a5.AchiId = Guid.NewGuid().ToString();
            this.achiLogic.AddAchi(a5);

            User u2 = new User() { Name = "LewiTT", Age = 17 };
            u2.UserId = Guid.NewGuid().ToString();
            this.userLogic.AddNewUser(u2);

            Game g2 = new Game() { Name = "Cyberpunk 2077", Genre = "RPG", UserId = u2.UserId, Rating = 5, GameTime = 150 };
            g2.GameId = Guid.NewGuid().ToString();
            this.gameLogic.AddGame(g2);

            Game g6 = new Game() { Name = "Csé gó", Genre = "Shooter", UserId = u2.UserId, Rating = 5, GameTime = 5420 };
            g6.GameId = Guid.NewGuid().ToString();
            this.gameLogic.AddGame(g6);

            Achievement a2 = new Achievement() { Name = "Hit nélkül", AchiLevel = AchiLevel.Silver, GameId = g2.GameId };
            a2.AchiId = Guid.NewGuid().ToString();
            this.achiLogic.AddAchi(a2);

            Game g3 = new Game() { Name = "Red Dead Redemption 2", Genre = "Adventure", UserId = u2.UserId, Rating = 5, GameTime = 80 };
            g3.GameId = Guid.NewGuid().ToString();
            this.gameLogic.AddGame(g3);

            Achievement a3 = new Achievement() { Name = "Semmit Nem csinaltal", AchiLevel = AchiLevel.Bronze, GameId = g2.GameId };
            a3.AchiId = Guid.NewGuid().ToString();
            this.achiLogic.AddAchi(a3);

            Game g12 = new Game() { Name = "Rocket league", Genre = "Racing", UserId = u2.UserId, Rating = 5, GameTime = 850 };
            g12.GameId = Guid.NewGuid().ToString();
            this.gameLogic.AddGame(g12);

            User u3 = new User() { Name = "Pleb", Age = 29 };
            u3.UserId = Guid.NewGuid().ToString();
            this.userLogic.AddNewUser(u3);

            Game g4 = new Game() { Name = "Csé gó", Genre = "Shooter", UserId = u3.UserId, Rating = 5, GameTime = 3200 };
            g4.GameId = Guid.NewGuid().ToString();
            this.gameLogic.AddGame(g4);

            Game g11 = new Game() { Name = "Rocket league", Genre = "Racing", UserId = u3.UserId, Rating = 5, GameTime = 950 };
            g11.GameId = Guid.NewGuid().ToString();
            this.gameLogic.AddGame(g11);

            Game g15 = new Game() { Name = "Tankos gem", Genre = "MMO", UserId = u3.UserId, Rating = 5, GameTime = 2500 };
            g15.GameId = Guid.NewGuid().ToString();
            this.gameLogic.AddGame(g15);

            User u4 = new User() { Name = "rtk", Age = 21 };
            u4.UserId = Guid.NewGuid().ToString();
            this.userLogic.AddNewUser(u4);

            Game g5 = new Game() { Name = "Csé gó", Genre = "Shooter", UserId = u4.UserId, Rating = 5, GameTime = 3800 };
            g5.GameId = Guid.NewGuid().ToString();
            this.gameLogic.AddGame(g5);

            Achievement a4 = new Achievement() { Name = "Global Elite ", AchiLevel = AchiLevel.Gold, GameId = g5.GameId };
            a4.AchiId = Guid.NewGuid().ToString();
            this.achiLogic.AddAchi(a4);

            User u5 = new User() { Name = "Viperov", Age = 21 };
            u5.UserId = Guid.NewGuid().ToString();
            this.userLogic.AddNewUser(u5);

            Game g7 = new Game() { Name = "Csé gó", Genre = "Shooter", UserId = u5.UserId, Rating = 5, GameTime = 2300 };
            g7.GameId = Guid.NewGuid().ToString();
            this.gameLogic.AddGame(g7);

            Game g13 = new Game() { Name = "Dota", Genre = "Moba", UserId = u5.UserId, Rating = 5, GameTime = 3000 };
            g13.GameId = Guid.NewGuid().ToString();
            this.gameLogic.AddGame(g13);

            User u6 = new User() { Name = "xxxnorbi", Age = 21 };
            u6.UserId = Guid.NewGuid().ToString();
            this.userLogic.AddNewUser(u6);

            Game g8 = new Game() { Name = "Csé gó", Genre = "Shooter", UserId = u6.UserId, Rating = 5, GameTime = 1200 };
            g8.GameId = Guid.NewGuid().ToString();
            this.gameLogic.AddGame(g8);

            Game g10 = new Game() { Name = "Rocket league", Genre = "Racing", UserId = u6.UserId, Rating = 5, GameTime = 950 };
            g10.GameId = Guid.NewGuid().ToString();
            this.gameLogic.AddGame(g10);

            User u7 = new User() { Name = "Rachman", Age = 21 };
            u7.UserId = Guid.NewGuid().ToString();
            this.userLogic.AddNewUser(u7);

            Game g14 = new Game() { Name = "Csé gó", Genre = "Shooter", UserId = u7.UserId, Rating = 5, GameTime = 1200 };
            g14.GameId = Guid.NewGuid().ToString();
            this.gameLogic.AddGame(g14);

            return this.RedirectToAction(nameof(this.Index));
        }

        /// <summary>
        /// User adding view .
        /// </summary>
        /// <returns> User adding view. </returns>
        [HttpGet]
        public IActionResult AddUser()
        {
            return this.View();
        }

        /// <summary>
        /// User adding view .
        /// </summary>
        /// <param name="u">the user that is going to get addied to the database.</param>
        /// <returns> User listing view.</returns>
        [HttpPost]
        public IActionResult AddUser(User u)
        {
            u.UserId = Guid.NewGuid().ToString();
            this.userLogic.AddNewUser(u);
            return this.RedirectToAction(nameof(this.ListUsers));
        }

        /// <summary>
        /// Deletes the user by the id parameter.
        /// </summary>
        /// <param name="id">the id of the user that is going to get deleted.</param>
        /// <returns> User listing view.</returns>
        public IActionResult DeleteUser(string id)
        {
            this.userLogic.DeleteUser(id);
            return this.RedirectToAction(nameof(this.ListUsers));
        }

        /// <summary>
        /// Lists all the users .
        /// </summary>
        /// <returns> view with all the users.</returns>
        public IActionResult ListUsers()
        {
            return this.View(this.userLogic.GetUsers());
        }

        /// <summary>
        /// Brings up the user editing view.
        /// </summary>
        /// /// <param name="id">the user's id.</param>
        /// <returns> User editing view.</returns>
        public IActionResult EditUser(string id)
        {
            return this.View(this.userLogic.GetUser(id));
        }

        /// <summary>
        /// Updates the user.
        /// </summary>
        /// <param name="u">the updated user.</param>
        /// <returns> view with all the users.</returns>
        [HttpPost]
        public IActionResult EditUser(User u)
        {
            this.userLogic.UpdateUser(u.UserId, u);

            return this.View(nameof(this.ListUsers), this.userLogic.GetUsers());
        }

        /// <summary>
        /// brings up the game adding page.
        /// </summary>
        /// <param name="id">new game's ID.</param>
        /// <returns> view with adding game .</returns>
        [HttpGet]
        public IActionResult AddGame(string id)
        {
            return this.View(nameof(this.AddGame), id);
        }

        /// <summary>
        /// adds the new game to the database.
        /// </summary>
        /// <param name="g">the new game.</param>
        /// <returns> view with hte user's game library.</returns>
        [HttpPost]
        public IActionResult AddGame(Game g)
        {
            User u = this.userLogic.GetUser(g.UserId);
            g.GameId = Guid.NewGuid().ToString();
            this.gameLogic.AddGame(g);

            return this.View(nameof(this.ListGames), u.GameLibrary);
        }

        /// <summary>
        /// Lists all the games from the database.
        /// </summary>
        /// <returns> view with all the games.</returns>
        public IActionResult ListAllGames()
        {
            List<Game> games = this.gameLogic.GetAllGames().ToList();
            foreach (var game in games)
            {
                game.PlayerName = this.userLogic.GetUser(game.UserId).Name;
            }

            return this.View(nameof(this.ListAllGames), games);
        }

        /// <summary>
        /// Lists all the games of a user.
        /// </summary>
        /// <param name="id">the users id.</param>
        /// <returns> view with all the games of a user.</returns>
        public IActionResult ListGames(string id)
        {
            User u = this.userLogic.GetUser(id);

            return this.View(u.GameLibrary.AsQueryable());
        }

        /// <summary>
        /// deletes game.
        /// </summary>
        /// <param name="id">the games id.</param>
        /// <returns> view with all remaining the games of the user.</returns>
        public IActionResult DeleteGame(string id)
        {
            Game g = this.gameLogic.GetGame(id);

            this.gameLogic.DeleteGame(id);

            return this.View(nameof(this.ListGames), this.userLogic.GetUser(g.UserId).GameLibrary);
        }

        /// <summary>
        /// deletes game.
        /// </summary>
        /// <param name="id">the games id.</param>
        /// <returns> view with all the games .</returns>
        public IActionResult DeleteGameFromAllGames(string id)
        {
            Game g = this.gameLogic.GetGame(id);

            this.gameLogic.DeleteGame(id);

            return this.View(nameof(this.ListGames), this.gameLogic.GetAllGames());
        }

        /// <summary>
        /// brings up the page to edit a game.
        /// </summary>
        /// <param name="id">the games id.</param>
        /// <returns> view with game editing .</returns>
        public IActionResult EditGame(string id)
        {
            return this.View(this.gameLogic.GetGame(id));
        }

        /// <summary>
        /// edits the game.
        /// </summary>
        /// <param name="g">the game.</param>
        /// <returns> view with the users game  library .</returns>
        [HttpPost]
        public IActionResult EditGame(Game g)
        {
            this.gameLogic.UpdateGame(g.GameId, g);

            return this.View(nameof(this.ListGames), this.userLogic.GetUser(g.UserId).GameLibrary);
        }

        /// <summary>
        /// brings up the achievement adding page.
        /// </summary>
        /// <param name="id">the game's id .</param>
        /// <returns> view with the achievement adding .</returns>
        public IActionResult AddAchievement(string id)
        {
            return this.View(nameof(this.AddAchievement), id);
        }

        /// <summary>
        /// adds an achievement to a game.
        /// </summary>
        /// <param name="a">the achievement .</param>
        /// <returns> view with the achievement adding .</returns>
        [HttpPost]
        public IActionResult AddAchievement(Achievement a)
        {
            Game g = this.gameLogic.GetGame(a.GameId);
            a.AchiId = Guid.NewGuid().ToString();
            this.achiLogic.AddAchi(a);

            return this.View(nameof(this.ListAchi), g.Achievements);
        }

        /// <summary>
        /// lists a game's achievements.
        /// </summary>
        /// <param name="id">the game's id .</param>
        /// <returns> view with the game's achievements .</returns>
        public IActionResult ListAchi(string id)
        {
            return this.View(nameof(this.ListAchi), this.gameLogic.GetGame(id).Achievements);
        }

        /// <summary>
        /// lists a game's achievements.
        /// </summary>
        /// <returns> view with the game's achievements .</returns>
        public IActionResult ListAchievements()
        {
            return this.View(nameof(this.ListAchi), this.achiLogic.GetAchievements());
        }

        /// <summary>
        /// deletes a game's achievement.
        /// </summary>
        /// /// <param name="id">the game's id .</param>
        /// <returns> view with the game's remaining achievements.</returns>
        public IActionResult DeleteAchi(string id)
        {
            Achievement a = this.achiLogic.GetAchievement(id);
            this.achiLogic.DeleteAchi(a);
            return this.View(nameof(this.ListAchi), this.gameLogic.GetGame(a.GameId).Achievements);
        }

        /// <summary>
        /// brings up the achievement editing page.
        /// </summary>
        /// /// <param name="id">the achievement's id .</param>
        /// <returns> view with the achievement editing.</returns>
        public IActionResult EditAchi(string id)
        {
            return this.View(this.achiLogic.GetAchievement(id));
        }

        /// <summary>
        /// edits the achievement .
        /// </summary>
        /// /// <param name="a">the achievement .</param>
        /// <returns> view with the game's achievements.</returns>
        [HttpPost]
        public IActionResult EditAchi(Achievement a)
        {
            this.achiLogic.UpdateAchi(a.AchiId, a);

            return this.View(nameof(this.ListAchi), this.gameLogic.GetGame(a.GameId).Achievements);
        }
    }
}