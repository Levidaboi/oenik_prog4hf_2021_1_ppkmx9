﻿// <copyright file="GameApiController.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Feleves_feladat.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using AutoMapper;
    using Feleves_feladat.Model;
    using Logic;
    using Microsoft.AspNetCore.Mvc;
    using Models;

    /// <summary>
    /// Documentation of the public GameApiController class.
    /// </summary>
    public class GameApiController : Controller
    {
        /// <summary>
        /// gamelogic instance.
        /// </summary>
        public GameLogic Logic;

        /// <summary>
        /// imapper instance.
        /// </summary>
        public IMapper mapper;

        /// <summary>
        /// Initializes a new instance of the <see cref="GameApiController"/> class.
        /// </summary>
        /// <param name="logic">dfd.</param>
        /// <param name="mapper">dfdf.</param>
        public GameApiController(GameLogic logic, IMapper mapper)
        {
            this.Logic = logic;
            this.mapper = mapper;
        }

        /// <summary>
        /// deletes a game.
        /// </summary>
        /// <returns>The joined names.</returns>
        [HttpGet]
        [ActionName("all")]
        public IEnumerable<GameModel> GetAll()
        {
            var games = this.Logic.GetAllGames();

            return this.mapper.Map<IQueryable<Models.Game>, List<GameModel>>(games);
        }

        /// <summary>
        /// deletes a game.
        /// </summary>
        /// <param name="id">The first name to join.</param>
        /// <returns>The joined names.</returns>
        [HttpGet]
        [ActionName("del")]
        public ApiResult DeleteGame(string id)
        {
            int i = this.Logic.GetAllGames().Count();
            this.Logic.DeleteGame(id);
            if (i > this.Logic.GetAllGames().Count())
            {
                return new ApiResult() { OperationResult = true };
            }

            return new ApiResult() { OperationResult = false };
        }

        /// <summary>
        /// add a game .
        /// </summary>
        /// <param name="game">Describes a game store which shall be inserted.</param>
        /// <returns>The result of an API call.</returns>
        [HttpPost]
        [ActionName("add")]
        public ApiResult AddGame(GameModel game)
        {
            try
            {
                Game game0 = new Game
                {
                     GameId = Guid.NewGuid().ToString(),
                     GameTime = game.GameTime,
                     Genre = game.Genre,
                     Name = game.Name,
                };
                this.Logic.AddGame(game0);
            }
            catch
            {
                return new ApiResult() { OperationResult = false };
            }

            return new ApiResult() { OperationResult = true };
        }

        /// <summary>
        /// Updates one knife store.
        /// </summary>
        /// <param name="game">Describes an game which shall be updated.</param>
        /// <returns>The result of an API call.</returns>
        [HttpPost]
        [ActionName("mod")]
        public ApiResult UpdateGame(GameModel game)
        {
            try
            {
                Game game0 = new Game
                {
                    GameId = game.GameId,
                    GameTime = game.GameTime,
                    Genre = game.Genre,
                    Name = game.Name,
                };
                this.Logic.UpdateGame(game.GameId, game0);
            }
            catch
            {
                return new ApiResult() { OperationResult = false };
            }

            return new ApiResult() { OperationResult = true };
        }
    }
}
